const sideLength = process.argv[2];
const area = sideLength**2;

console.log(`The area of a square with sides of length ${sideLength} has an area of ${area}`);