const daysPerYear = 365;
const hoursPerDay = 24;
const secondsPerHour = 60 * 60;
const secondsPerYear = secondsPerHour * hoursPerDay * daysPerYear;

console.log(`A year has ${secondsPerYear} seconds.`);