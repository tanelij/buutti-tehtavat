let playerCount, isStressed, hasIcecream
    , temperature, sunShining, raining, seesSuzy
    , seesDan;

if(playerCount === 4){
    console.log("The game of hearts can be played!!");
}

if(!isStressed || hasIcecream){
    console.log("Mark is happy");
}

if(sunShining && !raining && temperature >= 20){
    console.log("It is a beach day!");
}

if((seesSuzy || seesDan) && !(seesSuzy && seesDan)){
    console.log("Arin is happy!");
}