const a = 5;
const b = 7;

const sum = a + b;
const fraction = a / b;
const product = a * b;
const exponantiation = a**b;
const modulo = a % b;
const average = (a + b) / 2;


console.log(`sum = ${sum}`);
console.log(`fraction = ${fraction}`);
console.log(`product = ${product}`);
console.log(`exponantiation = ${exponantiation}`);
console.log(`modulo = ${modulo}`);
console.log(`average = ${average}`);
