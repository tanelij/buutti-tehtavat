const distance = 50;
const speed = 32;

const travelTime = distance / speed;

console.log(`Travel time is ${travelTime} hours`);