const price = 5;
const discount = 0.4;

const discountedPrice = price * (1 - discount);
console.log(`Discounted price is ${discountedPrice}`);