let input = process.argv[process.argv.length - 1];

if(!(checkWhiteSpace() && checkLength() && checkFirstLetter())){
    console.log("Invalid string!!!");
}

function checkWhiteSpace(){
    return input === input.trim();
}

function checkLength(){
    return input.length <= 20;
}

function checkFirstLetter(){
    const firstLetter = input.charAt(0);
    return firstLetter === firstLetter.toLowerCase();
}