import React from 'react'
import { useState } from 'react'
import ReactDOM from 'react-dom/client'
import App from './15.9/component.jsx'
import './index.css'

const SomeContext = React.createContext([{}, () => {}]); // Create context

const SomeProvider = props => { // Provider provides child components with props
    const [state, setState] = useState({name: "Seppo"}); //Props we want child components to have
    return (
        <SomeContext.Provider value={[state, setState]}>
            {props.children}
        </SomeContext.Provider>
    );
};

const Appi = () =>
    <SomeProvider><OtherComponent /></SomeProvider>; // Wrap child component with provider

const OtherComponent = () => {
    const [state, setState] = React.useContext(SomeContext); // Get state prop from context
    // setState("Jorma");
    return <div>{state.name}<button onClick={rename}>r</button></div>; // Get the name prop from state
};


ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
)
