const Component = ({list}) => {
    const elems = list.map((name, i) => {
        let nameElem;
        if((i+1) % 2 === 0){
            nameElem = <b>{name}</b>
        }else{
            nameElem = <i>{name}</i>
        }
        return (
            <ul key={name}>
                {nameElem}
            </ul>
        )
    });
    return (
        <div>{elems}</div>
    )
}

export default Component;