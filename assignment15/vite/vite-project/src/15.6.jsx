import { useState } from "react";

const Component = () => {
    const [str, setStr] = useState("");
    const [title, setTitle] = useState("");
    const changeHandler = (e) => {
        setStr(e.target.value);
    }
    const submitHandler = (e) => {
        e.preventDefault();
        setTitle(str);
        setStr("");
    }
    return (
        <div>
            <h2>Your string is: {title}</h2>
            <form onSubmit={submitHandler}>
                <input type="text" onChange={changeHandler} value={str}></input>
                <input type="submit"></input>
            </form>
        </div>
    )
}

export default Component;