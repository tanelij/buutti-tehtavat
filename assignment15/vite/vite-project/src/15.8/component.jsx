import { useState } from "react";
import "./style.css";

const boardSize = 3;
const winCount = 3;
const initialSymbol = "x";
const board = Array.from(function*(){
    for(let i=0; i<boardSize; i++){
        yield Array.from(function*(){
            for(let j=0; j<boardSize; j++){
                yield {
                    id: (i*boardSize) + j,
                    symbol: "",
                    x: j,
                    y: i
                }
            }
        }());
    }
}());

const columns = board[0].map((box, i) => {
    return Array.from(
        function*(){
            for(let y=0, x=i; y<board.length; y++){
                yield board[y][x]
            }
        }()
    )
});

const width = board[0].length;
const height = board.length;
function inBounds(x, y){
    return board[y] !== undefined
    && board[x] !== undefined;
}
function getDiagonal(xStart, yStart, xDelta, yDelta){
    return Array.from(function*(){
        for(let x=xStart, y=yStart; inBounds(x, y); x+=xDelta, y+=yDelta){
            yield board[y][x];
        }
    }());
}
const diagonals = Array.from(function*(){
    for(let x=0; x<width; x++){
        yield getDiagonal(x, 0, 1, 1);   
        yield getDiagonal(x, 0, -1, 1);
    }
    for(let y=1; y<height; y++){
        yield getDiagonal(0, y, 1, 1);
        yield getDiagonal(width, y, -1, 1);
    }
}()).filter(diagonal => diagonal.length >= winCount);
const rows = board;

function checkWin(playerSymbol){
    return [rows, columns, diagonals].some(arrays => {
        return arrays.some(array => {
            let counter = 0;
            return array.some(({symbol}) => {
                if(symbol === playerSymbol){
                    counter++;
                }else{
                    counter = 0;
                }
                return counter >= winCount;
            });
        });
    });
}

const Board = () => {
    const [symbol, setSymbol] = useState(initialSymbol);
    const [disabled, setDisabled] = useState(false);
    function makeMove(x, y){
        if(board[y][x].symbol !== "" || disabled){
            return;
        }

        board[y][x].symbol = symbol;
        const nextSymbol = symbol === "x" ? "o" : "x";
        setSymbol(nextSymbol);
        const playerWon = checkWin(symbol);
        if(playerWon){
            alert(`${symbol} WON!!!`);
            setDisabled(true);
        }
    }

    function reset(){
        board.flat().forEach(obj => {
            obj.symbol = "";
        });
        setSymbol(initialSymbol);
        setDisabled(false);
    }

    const rows = board.map((row, i) => {
        const boxes = row.map(({id, x, y, symbol: boxSymbol}) => {
            const clickHandler = () => {
                makeMove(x, y);
            }
            return <div onClick={clickHandler} key={id} className="box">{boxSymbol}</div>
        });
        return (
            <div key={i} className="row">
                {boxes}
            </div>
        )
    });

    const hasEmptyBoxes = board.some(row => {
        return row.some(({symbol}) => {
            return symbol === "";
        });
    });
    if(!hasEmptyBoxes){
        alert("GAME OVER!");
    }
    return (
        <div>
            <button onClick={reset}>Reset</button>
            <div className="board">
                <div>Current player: {symbol}</div>
                <div className="column">
                    {rows}
                </div>
            </div>
        </div>
    )
}

export default Board;