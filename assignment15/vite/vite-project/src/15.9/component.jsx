import { useState, useEffect } from "react";
import "./style.css";

function generateCards(amount){
    if(amount % 2 !== 0){
        throw "Amount must be a even number!";
    }
    let char = "a";
    let charCode = char.charCodeAt(0);

    const cards = Array.from(
        function*(){
            for(let i=0; i<amount; i+=2){
                for(let j=0; j<2; j++){
                    yield {
                        id: i+j,
                        symbol: char,
                        closed: true,
                        guessed: false
                    };
                }
                char = String.fromCharCode(++charCode);
            }
        }()
    );

    return Array.from(
        function*(){
            while(cards.length > 0){
                const randomIndex = Math.floor(Math.random() * cards.length);
                yield cards.splice(randomIndex, 1)[0];
            }
        }()
    );
}
const Game = () => {
    const [cards, setCards] = useState(generateCards(16));
    const options = Array.from(function*(){
        for(let i=2; i<=32; i+=2){
            yield {
                id: i,
                value: i
            };
        }
    }());
    
    function newGame(cardCount){
        setCards(generateCards(cardCount));
    }

    const optionElements = options.map(({id, value}) => {
        return <option key={id} value={value}>{value}</option>
    });
    const handleSelect = (e) => {
        const cardCount = e.target.value;
        newGame(cardCount);
    }

    return (
        <div>
            <select onChange={handleSelect}>
                {optionElements}
            </select>
            <Board cards={cards}></Board>
        </div>
    )
};

function gcd(n, start){
    let denominator=start;
    for(; denominator>1; denominator--){
        if(n % denominator === 0){
            break;
        }
    }
    return denominator;
}

const Board = ({cards}) => {
    const cardCount = cards.length;
    const rowCount = gcd(cardCount, Math.ceil(Math.sqrt(cardCount)));
    const [openCards, setOpenCards] = useState([]);
    let cardsDisabled = false;

    useEffect(() => {
        if(openCards.length === 2){
            const [card1, card2] = openCards;
            if(card1.symbol === card2.symbol){
                card1.guessed = true;
                card2.guessed = true;
                setOpenCards([]);
                return;
            }
            cardsDisabled = true;
            setTimeout(() => {
                cardsDisabled = false;
                openCards.forEach(card => card.closed = true);
                setOpenCards([]);
            }, 2000);
        }
    }, [openCards])


    const rows = cards.length > 0 ? Array.from(
        function*(){
            for(let i=0; i<rowCount; i++){
                const cardElements = Array.from(
                    function*(){
                        const cardsPerRow = cardCount / rowCount;
                        for(let j=0; j<cardsPerRow; j++){
                            const card = cards[i*cardsPerRow+j];
                            const {symbol , id, closed, guessed} = card;
                            const onClick = () => {
                                if(cardsDisabled || !closed || guessed){
                                    return;
                                }
                                card.closed = false;
                                setOpenCards([...openCards, card]);
                            }
                            const className = guessed ? "card guessed" : "card";
                            yield (
                            <div onClick={onClick} className={className} key={id}>
                                <span className={closed ? "hidden" : ""}>{symbol}</span>
                            </div>)
                        }
                    }()
                )
                yield <div key={i} className="row">{cardElements}</div>
            }
        }()
    ) : [];

    return (
        <div className="column">
            {rows}
        </div>
    )
}

export default Game;