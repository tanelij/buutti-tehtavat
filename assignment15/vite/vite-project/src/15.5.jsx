import { useState } from "react";

const Clicker = ({number, add}) => {
    const handleClick = () => {
        add(1);
    }

    return (
        <button onClick={handleClick}>{number}</button>
    )
}

const clickerCount = 5;
const initialState = Array.from(function*(count){
    for(let i=0; i<count; i++){
        yield [i, 0]
    }
}(clickerCount)).reduce((obj, [key, value]) => {
    obj[key] = value;
    return obj;
}, {});
const MainComponent = () => {
    const [values, setValues] = useState(initialState);
    function* makeClickers(count){
        for(let i=0; i<count; i++){
            const add = (value) => {
                setValues(
                    {
                        ...values,
                        [i]: (values[i] + value)
                    }
                )
            }
            const key = i;
            const number = values[key];
            yield <Clicker {...{key, number, add}}></Clicker>
        }
    }
    
    const clickers = Array.from(makeClickers(clickerCount));

    const total = Object.values(values).reduce((prev, cur) => {
        return prev + cur;
    }, 0);
    return (
        <div>
            {clickers}    
            <div>Total: {total}</div>
        </div>
    )
}

export default MainComponent;