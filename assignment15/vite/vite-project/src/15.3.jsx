// import { useState } from 'react'
// import './App.css'

const Hello = ({str}) => {
    return (
      <h1>{str}</h1>
    )
  }
  
  const CurrentYear = () => {
    const curYear = new Date().getFullYear();
    return (
      <h2>It is year {curYear}</h2>
    )
  }
  
  function App() {
    return (
  <div>
      <Hello str="Hello React!"></Hello>
      <CurrentYear></CurrentYear>
  </div>
    )
  }
  
  export default App
  