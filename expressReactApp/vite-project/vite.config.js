import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
// export default defineConfig({
//     plugins: [react()],
//     build: {
//       lib: {
//         // Could also be a dictionary or array of multiple entry points
//         entry: resolve(__dirname, 'lib/main.js'),
//         name: 'MyLib',
//         // the proper extensions will be added
//         fileName: 'my-lib',
//       }
//     }
// })

export default defineConfig({
  plugins: [react()]
})