import { useState, useEffect } from "react";

function App(){
  const [quote, setQuote] = useState("test quote");
  useEffect(() => {
    initialize();
  }, []);

  const initialize = async () => {
    const url = "http://localhost:3000/quote";
    const response = await fetch(url);
    const q = await response.text();
    setQuote(q);
  }

  return (
    <div className="App">
      <h1>The best FooBar page!</h1>
      <p>{quote}</p>
    </div>
  )
}

export default App
