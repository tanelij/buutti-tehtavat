import express from "express";
import cors from "cors";

const PORT = process.env.PORT || 3000;

const server = express();
//server.use(cors());
server.use("/", express.static("../vite-project/dist"));

server.get("/quote", (req, res) => {
    res.send("I love the smell of napalm in the morning.");
});

server.listen(PORT);