function generateCredentials(fName, lName){
    const yearStr = new Date().getFullYear().toString().slice(-2);
    const nameStr = [lName, fName].map((str) => {
        return str.slice(0, 2).toLowerCase();
    }).join("");
    const uName = "B" + yearStr + nameStr;

    const randomLetter = String.fromCharCode(getRandomFromRange(65, 90));
    const randomSpecChar = String.fromCharCode(getRandomFromRange(33, 47));
    const middle = fName.slice(0, 1).toLowerCase()
        + lName.slice(-1).toUpperCase();
    const password = randomLetter + middle + randomSpecChar + yearStr;

    function getRandomFromRange(min, max){
        const range = max - min;
        
        return min + Math.floor((range + 1) * Math.random());
    }
    return [uName, password];
}

console.log(generateCredentials("John", "Doe")); // [ 'B22dojo', 'KjE,22' ]
