import fs from "fs";

fs.readFile("./test.txt", "utf-8", (err,file) => {
    if(err){
        console.log("Error");
    }else{
        console.log(file);
        console.log();
        let newText = file.replaceAll("joulu", "kinkku")
            .replaceAll("lapsilla", "poroilla")
            .replaceAll("Joulu", "Kinkku")
            .replaceAll("Lapsilla", "Poroilla");
        console.log(newText);

        fs.writeFile("./new-file.txt", newText, (err) => {
            if(err){
                console.log("Error");
            }else{
                console.log("Success!!");
            }
        });
    }
});

