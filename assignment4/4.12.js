function collatz(n){
    let i = 0;
    while(n !== 1){
        if(n % 2 === 0){
            n /= 2;
        }else{
            n = (n * 3) + 1; 
        }
        i++;
    }
    return i;
}

console.log(collatz(3));