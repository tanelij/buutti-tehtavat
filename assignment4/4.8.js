function aboveAverage(arr){
    const average = arr.reduce((prev, next) => {
        return prev + next;
    }) / arr.length;

    console.log(`Average: ${average}`);
    return arr.filter((num) => {
        return num > average;
    });
}

console.log(aboveAverage([1, 5, 9, 3]));