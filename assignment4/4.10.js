function checkExam(correctAnswers, studentAnswers){
    return correctAnswers.reduce((score, answer, i) => {
        const studentAnswer = studentAnswers[i];
        // console.log("studentAnswer ", studentAnswer);
        // console.log("answer ", answer);

        switch(studentAnswer){
        case(answer):
            return score + 4;
        case(""):
            return score;
        default:
            return score - 1;
        }
    }, 0);
}

[
    [["a", "a", "b", "b"], ["a", "c", "b", "d"]],
    [["a", "a", "c", "b"], ["a", "a", "b",  ""]],
    [["a", "a", "b", "c"], ["a", "a", "b", "c"]],
    [["b", "c", "b", "a"], ["",  "a", "a", "c"]]
].forEach(([answerArr, studentArr]) => {
    console.log(checkExam(answerArr, studentArr));
});