function likes(arr){
    const likeStr = arr.length < 2
        ? "likes this"
        : "like this";

    if(arr.length === 0){
        arr = ["No one"];
    }
    if(arr.length > 2){
        const firstTwo = arr.splice(0, 2);
        if(arr.length > 1){
            arr = [firstTwo.join(", "), `${arr.length} others`]
        }else{
            arr = [firstTwo.join(", "), arr[0]];
        }
    }

    return arr.join(" and ") + " " + likeStr;
}

[
    likes([]), // "no one likes this"
    likes(["John"]), // "John likes this"
    likes(["Mary", "Alex"]), // "Mary and Alex like this"
    likes(["John", "James", "Linda"]), // "John, James and Linda like this"
    likes(["Alex", "Linda", "Mark", "Max"]), // must be "Alex, Linda and 2 others 
    likes(["Alex", "John", "Mark", "Max", "Mark"]) // must be "Alex, John and 3 others 
].forEach((str) => {
    console.log(str);
});