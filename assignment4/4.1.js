function Ingredient(name, amount){
    this.name = name;
    this.amount = amount;
    this.toString = function(){
        return `{
\tname: ${this.name}
\tamount: ${this.amount}
}`;
    };
}

function Recipe(name, ingredients, servings){
    this.name = name;
    this.ingredients = ingredients;
    this.servings = servings;
    this.toString = function(){
        const ingredientsString = `[
${this.ingredients.join(",\n")}
]`;

        return `name: ${this.name}
servings: ${this.servings}
ingredients: ${ingredientsString}`;
    };
}

const liha = new Ingredient("liha", 2);
const mausteet = new Ingredient("mausteet", 5);
const keitto = new Recipe("lihakeitto", [liha, mausteet], 4);

console.log(keitto.toString());