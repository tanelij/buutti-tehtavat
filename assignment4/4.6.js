function calculator(operator, num1, num2) {
    let result;
    switch(operator){
    case("+"):
        result = num1 + num2;
        break;
    case("-"):
        result = num1 - num2;
        break;
    case("*"):
        result = num1 * num2;
        break;
    case("/"):
        result = num1 / num2;
        break;
    default:
        result = "Can't do that!";
        break;
    }
    return result;
}

[
    ["+", 2, 3],
    ["-", 2, 3],
    ["*", 2, 3],
    ["/", 2, 3],
    ["a", 2, 3]
].forEach((arr) => {
    console.log(calculator(...arr));
});