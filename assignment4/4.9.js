function getVowelCount(str){
    const vowels = ["a", "e", "i", "o", "u", "y"];
    return vowels.reduce((count, vowel) => {
        return count + Array.from(str.matchAll(vowel)).length;
    }, 0);
}
const word = "abracadabra";
const vowelCount = getVowelCount(word);
console.log(`The word "${word}" has ${vowelCount} vowels.`);