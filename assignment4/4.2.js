function Ingredient(name, amount){
    this.name = name;
    this.amount = amount;
    this.toString = function(){
        return `{
\tname: ${this.name}
\tamount: ${this.amount}
}`;
    };
}

Ingredient.prototype.scale = function(factor){
    this.amount *= factor;
};

function Recipe(name, ingredients, servings){
    this.name = name;
    this.ingredients = ingredients;
    this.servings = servings;
}

Recipe.prototype.toString = function(){
    const ingredientsString = `[
${this.ingredients.join(",\n")}
]`;

    return `name: ${this.name}
servings: ${this.servings}
ingredients: ${ingredientsString}`;
};

Recipe.prototype.setServings = function(amount){
    const factor = amount / this.servings;
    this.ingredients.forEach(ingredient => {
        ingredient.scale(factor);
    });
    this.servings = amount;
};

const liha = new Ingredient("liha", 2);
const mausteet = new Ingredient("mausteet", 5);
const keitto = new Recipe("lihakeitto", [liha, mausteet], 4);

console.log(keitto.toString());
liha.scale(2);
console.log(keitto.toString());
keitto.setServings(100);
console.log(keitto.toString());

export {Recipe, Ingredient};