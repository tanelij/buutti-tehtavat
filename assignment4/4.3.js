import {Recipe, Ingredient} from "./4.2.js";

class HotRecipe extends Recipe{
    constructor(name, ingredients, servings, heatLevel){
        super(name, ingredients, servings);
        this.heatLevel = heatLevel;
    }

    toString(){
        let str = super.toString();
        str += `\nheat level: ${this.heatLevel}`;
        
        if(this.heatLevel > 5){
            str += `\nWARNING!!! Heat level over 5`;
        }
        return str;
    }
}

const liha = new Ingredient("liha", 2);
const mausteet = new Ingredient("mausteet", 5);
const keitto = new Recipe("lihakeitto", [liha, mausteet], 4);
const tulinenKeitto = new HotRecipe(keitto.name, keitto.ingredients, keitto.servings, 6);

console.log(tulinenKeitto.toString());
