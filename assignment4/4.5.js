import fs from "fs";

try{
    const file = fs.readFileSync("./forecast.json", "utf-8");

    console.log(file);

    const forecast = JSON.parse(file);
    forecast.temperature += 5;

    fs.writeFile("./forecast.json", JSON.stringify(forecast), (err) => {
        if(err){
            console.log("Error");
        }else{
            console.log("Success!!");
        }
    });
}catch(error){
    console.log(error);
}