function getLotteryNumbers(n){
    const maxNumber = 40;
    const arr = Array.from(Array(maxNumber), (v, k) => {
        return k+1;
    });

    return Array.from(Array(n), (v, k) => {
        return Math.floor(Math.random() * (maxNumber + 1 - k));
    }).map((random) => {
        return arr.splice(random, 1)[0];
    });
}


console.log(getLotteryNumbers(7));