
const arr1 = [1, 2, 3, 4, 4];
const arr2 = [3, 4, 5, 6, 6];

function joinArrays(arr1, arr2){
    const arr3 = [];
    [...arr1, ...arr2].forEach(x => {
        if (!arr3.includes(x))
            arr3.push(x);
    });
    return arr3;
}

function joinArrays2(arr1, arr2){
    return [...arr1, ...arr2].filter((x, i, arr) => {
        return !arr.slice(0,i).includes(x);
    });
} 

console.log(joinArrays(arr1, arr2));
console.log(joinArrays2(arr1, arr2));


