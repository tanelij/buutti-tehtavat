const arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22];

console.log(`Original array: 
${arr}`);

const arr1 = arr.filter(n => n % 3 === 0);
console.log(`Divisible by 3:
${arr1}`);

const arr2 = arr.map(n => n * 2);
console.log(`Array multiplied by 2:
${arr2}`);

const arr3 = arr.reduce((prev, cur) => prev + cur);
console.log(`Sum of values: ${arr3}`);