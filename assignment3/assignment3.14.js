import {getInput} from "../helpers.js";

const [start, end] = getInput(2).map(x => parseInt(x));

const ascending = start < end;

const arr = [];

if(ascending){
    for(let i=start; i<=end; i++){
        arr.push(i);
    }
}else{
    for(let i=start; i>=end; i--){
        arr.push(i);
    }
}
console.log(arr);