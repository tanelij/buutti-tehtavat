const str = process.argv[2];

const words = str.split(" ").map((w) => {
    return w.split("").map((c, i) => {
        return w[w.length - (i+1)];
    }).join("");
}).join(" ");

console.log(words);