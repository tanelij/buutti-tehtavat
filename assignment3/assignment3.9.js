function isPrime(n){
    for(let i=2; i<n; i++){
        if(n % i === 0){
            return false;
        }
    }

    return true;
}

for(let i=1; i<20; i++){
    const str = isPrime(i) ? "is prime!" : "is not prime :(";
    console.log(`${i} ${str}`);
}