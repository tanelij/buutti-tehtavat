function getRunwayNumber(bearing){
    return Math.round(bearing / 10);
}

function getRunwayNumber2(bearing){
    const roundedBearing = Math.round(bearing / 10) * 10;
    return roundedBearing / 10;
}

// function getRunwayNumber3(bearing){
//     const roundedToHundred = bearing / 100;
    
//     return roundedBearing / 10;
// }

console.log(getRunwayNumber(267.5));
console.log(getRunwayNumber2(267.5));