function toUpperCase(str){
    const words = str.split(" ");
    return words.map((w) => {
        return w.slice(0, 1).toUpperCase() + w.slice(1);
    }).join(" ");
}

console.log(toUpperCase("this is a Test string to test this program."));