const competitors = ["Julia", "Mark", "Spencer", "Ann" , "John", "Joe"];
const ordinals = ["st", "nd", "rd", "th"];

const result = competitors.map((name, i) => {
    const ordinalIndex = i < ordinals.length ? i : ordinals.length-1;
    return `${i+1}${ordinals[ordinalIndex]} competitor was ${name}`;
});

console.log(result);