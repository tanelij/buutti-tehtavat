const word = process.argv[2];

const reversedWord = word.split("").map((c, i)=> {
    return word[word.length - (i+1)];
}).join("");

const isPalindrome = reversedWord === word;
const str = isPalindrome ? 
    `Yes, ${word} is a palindrome` :
    `No, ${word} is not a palindrome`;

console.log(str);