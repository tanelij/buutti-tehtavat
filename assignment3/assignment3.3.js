
function factorial(n){
    if(n > 1){
        return factorial(n - 1) * n;
    }
    return n;
}

console.log(factorial(4));