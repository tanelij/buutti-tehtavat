const str = process.argv[2];

const indexStr = str.split("").map(c => {
    const refCode = "a".charCodeAt(0);
    return c.charCodeAt(0) - (refCode - 1);
}).join("");

console.log(indexStr);

