const input = "aabboohoooofffkkccjdddTTT";

function nonRepeatingChar(str){
    const arr = str.split("");

    return arr.find((c, i) => {
        const prev = arr[i - 1];
        const next = arr[i + 1];

        return prev !== c && next !== c;
    });
}

function nonRepeatingChar2(str){
    const arr = str.split("");

    return arr.find((c, i) => {
        if(arr[i+1] === c){
            return false;
        }
        return arr[i-1] !== c;
    });
}
console.log(nonRepeatingChar(input));
console.log(nonRepeatingChar2(input));