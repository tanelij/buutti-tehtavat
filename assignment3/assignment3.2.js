import {getInput} from "../helpers.js";
/**
 * A function for generating a random integer within a range.
 * @param {Number} min minimum value for the generated number
 * @param {Number} max maximum value for the generated number
 * @returns A random integer within the supplied range.
 */
function getRandomNumber(min, max){
    const range = max - min;
    const rand = Math.random();

    return Math.round(min + (rand * (range + 1)));
}

const [min, max] = getInput(2).map(s => parseInt(s));
const rand = getRandomNumber(min, max);

console.log(`Random number with range ${min} - ${max}: ${rand}`);