
function fibonacci(n){
    const arr = Array(n).fill(0);
    
    arr[1] = 1;
    arr.slice(1).forEach((x, i) => {
        arr[i+2] = arr[i] + arr[i+1];
    });

    return arr;
}

console.log(fibonacci(7));