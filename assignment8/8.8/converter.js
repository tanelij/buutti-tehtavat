const factors = [
    ["l", 1],
    ["dl", 10],
    ["oz", 33.814],
    ["cup", 4.16667],
    ["pint", 2.11338]
].reduce((obj, [unit, factor]) => {
    obj[unit] = factor;
    return obj;
}, {});

function convert(amount, unit1, unit2){
    const litres = amount / factors[unit1];
    return litres * factors[unit2];
}
const amount = parseFloat(process.argv[2]);
const [unit, target] = Object.values(process.argv).slice(3);

console.log(convert(amount, unit, target));

export {convert};