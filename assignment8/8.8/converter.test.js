import {convert} from "./converter.js";

test("Should convert litres properly", () => {
    const testArr = [
        [12, "l", 12],
        [4, "dl", 40],
        [7.3, "oz", 246.8422],
        [3, "cup", 12.5],
        [0.3, "pint", 0.634013]
    ];
    const maxDelta = 1E-3;
    testArr.forEach(([amount, target, answer]) => {
        const value = convert(amount, "l", target);
        const delta = Math.abs(value - answer);
        expect(delta < maxDelta).toBe(true);
    });
});
