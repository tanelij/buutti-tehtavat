import {calculator} from "./calculator";

// test("2 + 3 = 5", () => {
//     const result = calculator("+", 2, 3);
//     expect(result).toBe(5);
// })

test("2 - 3 = -1", () => {
    const result = calculator("-", 2, 3);
    expect(result).toBe(-1);
});

test("2 * 3 = 6", () => {
    const result = calculator("*", 2, 3);
    expect(result).toBe(6);
});

// test("2 / 3 = 2/3", () => {
//     const result = calculator("/", 2, 3);
//     expect(result).toBe(2/3);
// })

test("2 a 3 = Invalid operator", () => {
    const result = calculator("a", 2, 3);
    expect(result).toBe("Invalid operator");
});

describe("Sum", () => {
    it("Should return 4 with params 1 and 3", () => {
        const result = calculator("+", 1, 3);
        expect(result).toBe(4);
    });

    it("Should throw an error with string parameter", () => {
        expect(() => {
            calculator("+", "1", 3);

        }).toThrow();
    });
});

describe("Division", () => {
    it("Should return 0.25 with params 1/4", () => {
        const result = calculator("/", 1, 4);
        expect(result).toBe(0.25);
    });

    it("Should round properly", () => {
        const delta = 1E-5;
        const result = calculator("/", 1, 1E+5);
        expect((result - 1E-5) < delta).toBe(true);
    });

    it("Should return zero if the first parameter is zero", () => {
        [
            1, -1, 0.00000001, 0.999999999
        ].forEach(number => {
            const result = calculator("/", 0, number);
            const expectedResult = number < 0 
                ? -0
                : 0;
            expect(result).toBe(expectedResult);
        });
    });
});