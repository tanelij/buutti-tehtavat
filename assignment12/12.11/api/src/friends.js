import express from "express";

const router = express.Router();
router.use(express.json());
const friends = new Map();

router.post("/", (req, res) => {
    const params = ["username", "name", "email"];

    try{
        const user = params.reduce((obj, paramName) => {
            const value = req.body[paramName];
            if(value === undefined || value.length === 0){
                throw `Missing param ${paramName}`;
            }
            obj[paramName] = value;
            return obj;
        }, {});
        user.friends = [];

        if(friends.has(user.username)){
            throw `User with username ${user.username} already exists`;
        }

        friends.set(user.username, user);
        res.status(204).send();
    }catch(error){
        res.status(400).send(error);
    }
});

function getUser(req, param){
    const user = friends.get(req.params[param]);
    if(user === undefined){
        throw `No user with username: ${req.params.username}`;
    }
    return user;
}

router.post("/add/:user1/:user2", (req, res) => {
    try{
        const user1 = getUser(req, "user1");
        const user2 = getUser(req, "user2");
        user1.friends.push(user2);
        res.status(204).send();
    }catch(error){
        res.status(404).send(error);
    }
});

router.get("/", (req, res) => {
    res.json(Array.from(friends.values()));
});

router.get("/:username", (req, res) => {
    try{
        const user = getUser(req, "username");
        res.json(user);
    }catch(error){
        res.status(404).send(error);
    }
});


export default router;

