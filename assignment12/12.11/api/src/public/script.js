const friendsTable = document.getElementById("friends-table");
const testTable = [
    {
        "username": "user1",
        "name": "seppo",
        "email": "seppo@gmail.com",
        "friends": [
            {
                "username": "user2",
                "name": "seppo",
                "email": "seppo@gmail.com",
                "friends": []
            }
        ]
    },
    {
        "username": "user2",
        "name": "seppo",
        "email": "seppo@gmail.com",
        "friends": []
    }
];

function fillTable(friends){
    const fields = ["username", "name", "email", "friends"];
    const tr = document.createElement("tr");
    friendsTable.appendChild(tr);
    fields.forEach(field => {
        const th = document.createElement("th");
        th.innerText = field;
        tr.appendChild(th);
    });
    friends.forEach(friend => {
        const tr = document.createElement("tr");
        friendsTable.appendChild(tr);
        fields.forEach(field => {
            const td = document.createElement("td");
            const value = field === "friends"
                ? friend[field].map(friend => friend.username).join("\n")
                : friend[field];

            td.innerText = value;
            tr.appendChild(td);
        });
    });
}

//fillTable(testTable);

fetch("/friends/").then((result) => {
    return result.json();
}).then(json => {
    fillTable(json);
});