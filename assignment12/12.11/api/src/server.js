import express from "express";
import router from "./friends.js";
import path from "node:path";
import * as url from "url";
const __dirname = url.fileURLToPath(new URL(".", import.meta.url));

const server = express();
server.use("/friends", router);
const publicPath = path.join(__dirname, "public");
server.use(express.static(publicPath));


export default server;