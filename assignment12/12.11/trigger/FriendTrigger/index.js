const fetch = require("node-fetch");
module.exports = async function (context, myTimer) {
    const timeStamp = new Date().toISOString();
    context.log("JavaScript timer trigger function ran!", timeStamp);   
    fetch("https://taneli-friends-app.azurewebsites.net/friends/add/user1/user2", {
        method: "POST"
    }).then((result) => {
        context.log(result);
    });
};