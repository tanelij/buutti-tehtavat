import express from "express";

const server = express();

server.get("/", (req, res) => {
    res.send("ok");
});

const port = process.env.PORT || 3000;

server.listen(port, () => {
    console.log(`Server listening to port ${port}`);
});