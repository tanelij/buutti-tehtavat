module.exports = async function (context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');
    const message = typeof(req.body) === "string" 
        ? req.body.toUpperCase()
        : "Error"
    context.res = {
        // status: 200, /* Defaults to 200 */
        body: message
    };
}