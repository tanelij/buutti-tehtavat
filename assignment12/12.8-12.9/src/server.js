import express from "express";

const server = express();

server.get("/random", (req, res) => {
    let {min, max, isInteger: isinteger} = req.query;
    min = parseInt(min);
    min = isNaN(min) ? 0 : min;
    isinteger = isinteger !== undefined;
    max = parseInt(max);
    max = isinteger ? max + 1 : max;

    let number = Math.random();

    if(!isNaN(max)){
        number *= ((max+1) - min);
    }
    if(!isNaN(min)){
        number += min;
    }
    if(isinteger){
        number = Math.floor(number);
    }
    res.send(number.toString());
});

export default server;