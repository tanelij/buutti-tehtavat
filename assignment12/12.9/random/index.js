module.exports = async function (context, req) {
    let {min, max, isinteger} = req.query;
    min = parseInt(min);
    min = isNaN(min) ? 0 : min;
    isinteger = isinteger !== undefined;
    max = parseInt(max);
    max = isinteger ? max + 1 : max;

    let number = Math.random();

    if(!isNaN(max)){
        number *= ((max+1) - min);
    }
    if(!isNaN(min)){
        number += min;
    }
    if(isinteger){
        number = Math.floor(number);
    }
    context.res = {
        // status: 200, /* Defaults to 200 */
        body: number.toString()
    };
};