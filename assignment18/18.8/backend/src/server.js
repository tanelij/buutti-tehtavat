import express, {Router} from "express";
import dao from "./db/dao.js";

const server = express();
server.use("/", express.static("./dist"));
const songsRouter = Router();
const PORT = process.env.PORT || 3000;

server.use("/songs", songsRouter);

songsRouter.get("/", async (req, res) => {
    const result = await dao.getSongs();
    res.json(result.rows);
});

songsRouter.get("/:id", async (req, res) => {
    const result = await dao.getSong(req.params.id);

    if(result.rows.length === 0){
        return res.status(404).send();
    }
    res.json(result.rows[0]);
});

server.listen(PORT, () => {
    console.log(`Server listening on port: ${PORT}`);
});