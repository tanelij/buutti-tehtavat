import executeQuery from "./db.js";
import queries from "./queries.js";

const dao = {
    getSong: async function(id){
        return await executeQuery(queries.get_song, id);
    },
    getSongs: async function(){
        return await executeQuery(queries.get_songs);
    }
};

export default dao;