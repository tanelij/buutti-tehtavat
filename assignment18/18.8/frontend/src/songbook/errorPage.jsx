import { useRouteError, Link } from "react-router-dom";

const ErrorPage = ({heading, message}) => {
    return (
        <div>
            <h1>404 Not found!</h1>
            <p>The requested resource is not here!</p>
            <Link to="/">Main page</Link>
        </div>
    )
}

export default () => {
    const error = useRouteError();
    switch(error.status){
        case(404):
            return (
                <ErrorPage 
                heading="404 Not found!" 
                message="The requested resource is not here!">
                </ErrorPage>
            )
        default:
            return (
                <ErrorPage 
                heading="An unexpected error occurred!" 
                message={":("}>
                </ErrorPage>
            )
    }
}
