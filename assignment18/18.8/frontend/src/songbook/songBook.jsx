import {useState} from "react";
import { createBrowserRouter, RouterProvider, useLoaderData } from "react-router-dom";
import {Link, Outlet, NavLink} from "react-router-dom";
import ErrorPage from "./errorPage.jsx";
import "./style.css";


const songLoader = async ({params}) => {
    const result = await fetch(`/songs/${params.id}`, {method: "GET"});
    const song = await result.json();

    if(song === undefined){
        const error = new Error();
        error.status = 404;
        throw error;
    }
    return song;
}

const songListLoader = async ({params}) => {
    const result = await fetch("/songs", {method: "GET"});
    const songs = await result.json();
    if(songs === undefined){
        const error = new Error();
        error.status = 500;
        throw error;
    }
    return songs;
}

const App = () => {
    const [searchStr, setSearchStr] = useState("");
    const handleSearchChange = (e) => {
        setSearchStr(e.target.value);
    }
    const songs = useLoaderData();
    const songLinks = songs.slice(0, 5)
    .filter(({title}) => title.toLowerCase().includes(searchStr.toLowerCase()))
    .map(({id, title}) => {
        return (
            <NavLink className={({isActive, isPending}) => {
                return "nav-link" + (
                    isActive 
                    ? " active"
                    : isPending
                    ? " pending"
                    : ""
                )
            }} to={`/${id}`} key={id}>{title}</NavLink>
        )
    });
    return (
        <div>
            <nav className="nav">
                <div className="song-container">{songLinks}</div>
                <input placeholder="Filter songs" className="search-input" value={searchStr} type="search" onChange={handleSearchChange}></input>
            </nav>
            <Outlet></Outlet>
        </div>
    )
}

const Songs = () => {
    const song = useLoaderData();

    const listElements = Object.entries(song)
    .filter(([key, value]) => key !== "id")
    .map(([key, value]) => {
        return (
            <li key={key}>{`${key}: ${value}`}</li>
        );
    });

    return (
        <ul id="song-data">
            {listElements}
        </ul>
    )
}

const router = createBrowserRouter([
    {
        element: <App></App>,
        path: "/",
        errorElement: <ErrorPage></ErrorPage>,
        loader: songListLoader,
        children: [
            {
                element: <Songs></Songs>,
                path: ":id",
                loader:  songLoader
            }
        ]
    }
]);

export default () => {
    return (
        <RouterProvider router={router}>
        </RouterProvider>
    )
}