import {describe, it, expect} from "vitest";
import {render, screen, getByText, prettyDOM} from "@testing-library/react";
import userEvent from '@testing-library/user-event';
import {within} from "@testing-library/dom";
import {vi} from "vitest";
import {Request} from "node-fetch";
import songs from "./songs.js";

const requestMock = vi.fn(function(){
    return new Request(...arguments);
})

vi.stubGlobal("Request", requestMock);

function mockFetch(result){
    vi.stubGlobal("fetch", vi.fn(() => {
        return {
            json: async () => {
                return result;
            }
        }
    }));
}

mockFetch(songs);
import SongBook from "../src/songbook/songBook.jsx";

describe("Main page", () => {
    function getSongLinks(){
        return songs.map(({title}) => {
            return getLink(title);
        });
    }
    function getLink(title){
        const navBar = screen.getByRole("navigation");
        return getByText(navBar, new RegExp(`^${title}$`, "i"));
    }

    it("Should render navbar with songs", async () => {
        //mockFetch(songs);
        render(<SongBook></SongBook>);
        const links = getSongLinks();
        links.forEach(link => {
            expect(link).toBeInTheDocument();
        });
    });

    it("Should display the song after clicking the song link", async () => {
        mockFetch(songs);
        render(<SongBook></SongBook>);
        const user = userEvent.setup();

        for(let i=0; i<songs.length; i++){
            const song = songs[i];
            mockFetch(song);
            const {title, lyrics} = song;
            const link = getLink(title);
            await user.click(link);
            //console.log(prettyDOM());
            const songContainer = document.getElementById("song-data");
            const titleElement = getByText(songContainer, new RegExp(`title: ${title}`, "i"));
            expect(titleElement).toBeInTheDocument();
            const lyricsElement = within(songContainer).getByText((content, element) => {
                return content.includes(lyrics);
            }, {normalizer: (str) => str});
            expect(lyricsElement).toBeInTheDocument();
        }
    });

    it("Should filter songs when typing in search bar", async () => {
        mockFetch(songs);
        render(<SongBook></SongBook>);
        const user = userEvent.setup();
        const searchBox = screen.getByRole("searchbox");
        let links = getSongLinks();
        expect(links.length).toBe(5);
        links.forEach(link => {
            expect(link).toBeInTheDocument();
        });
        
        await user.click(searchBox);
        await user.keyboard("hoosianna");

        const navBar = screen.getByRole("navigation");
        links = within(navBar).queryAllByRole("link");
        //console.log(prettyDOM());
        const matchingSongs = [songs[0], songs[2]];
        expect(links.length).toBe(matchingSongs.length);
        const hasMatchingSongs = matchingSongs.every(({title}) => {
            return links.find(link => {
                return link.innerHTML === title;
            }) !== undefined;
        });
        expect(hasMatchingSongs).toBe(true);
    })
});