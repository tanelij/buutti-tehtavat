/* eslint-disable linebreak-style */
import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [react()],
    test: {
        globals: true,
        environment: "jsdom",
        setupFiles: "./test/setup.js"
    },
    server: {
        proxy: {
            "/songs": "http://localhost:3000"
        }
    }
});
