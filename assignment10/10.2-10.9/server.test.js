import request from "supertest";
import dotenv from "dotenv/config";
const secret = process.env.SECRET;
import server from "./server";
import jwt from "jsonwebtoken";


// describe("/students", () => {
//     it("Returns 404 on invalid address", async () => {
//         const response = await request(server)
//             .get("/students/invalidaddress");
//         expect(response.statusCode).toBe(404);
//     });

//     it("Should return status 401 without valid token", async () => {
//         const response = await request(server)
//             .get("/students/");

//         expect(response.statusCode).toBe(401);
//     });
// });

describe("/user/register", () => {
    it("Returns 404 on invalid address", async () => {
        const response = await request(server)
            .get("/user/register/invalidaddress");
        expect(response.statusCode).toBe(404);
    });

    it("Should return 401 request body doesn't contain necessary params", async () => {
        const invalidParams = [
            {
                username: "user"
            },
            {
                password: "wordPass"
            },
            {
                random: "random"
            }
        ];
        invalidParams.forEach(async (body) => {
            const response = await request(server)
                .post("/user/register")
                .send(JSON.stringify(body));

            expect(response.statusCode).toBe(401);
        });
    });

    it("Should return a valid token on register", async () => {
        const password = "password";
        const username = "user";
        const body = {
            username,
            password
        };
        const response = await request(server)
            .post("/user/register")
            .set("Content-type", "application/json")
            .set("Accept", "application/json")
            .send(body);
        expect(response.statusCode).toBe(201);

        const token = response.text;
        expect(() => {
            jwt.verify(token, secret);
        }).not.toThrow();
    });
});

describe("/user/login", () => {
    it("Returns 404 on invalid address", async () => {
        const response = await request(server)
            .get("/user/login/invalidaddress");
        expect(response.statusCode).toBe(404);
    });

    it("Should return 401 request body doesn't contain necessary params", async () => {
        const invalidParams = [
            {
                username: "user"
            },
            {
                password: "wordPass"
            },
            {
                random: "random"
            }
        ];
        invalidParams.forEach(async (body) => {
            const response = await request(server)
                .get("/user/login")
                .send(JSON.stringify(body));

            expect(response.statusCode).toBe(401);
        });
    });

    
    async function registerUser(body){
        return await request(server)
            .post("/user/register")
            .set("Content-type", "application/json")
            .set("Accept", "application/json")
            .send(body);
    }

    it("Should return 401 on non matching password", async() => {
        const password = "password";
        const username = "user";
        const body = {
            username,
            password
        };
        const bodyWithFalsePassword = {
            username,
            "password": "falsePassword"
        };
        registerUser(body);

        const response = await request(server)
            .get("/user/login")
            .set("Content-type", "application/json")
            .set("Accept", "application/json")
            .send(bodyWithFalsePassword);
        expect(response.statusCode).toBe(401);
    });

    it("Should return 401 on non existant user", async() => {
        const password = "password";
        const username = "user5";
        const body = {
            username,
            password
        };

        const response = await request(server)
            .get("/user/login")
            .set("Content-type", "application/json")
            .set("Accept", "application/json")
            .send(body);
        expect(response.statusCode).toBe(401);
    });

    it("Should return a valid token on login", async () => {
        const password = "password";
        const username = "user";
        const body = {
            username,
            password
        };
        registerUser(body);

        const response = await request(server)
            .get("/user/login")
            .set("Content-type", "application/json")
            .set("Accept", "application/json")
            .send(body);
        expect(response.statusCode).toBe(200);

        const token = response.text;
        expect(() => {
            jwt.verify(token, secret);
        }).not.toThrow();
    });
});