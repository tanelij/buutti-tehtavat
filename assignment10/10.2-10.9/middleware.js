import jwt from "jsonwebtoken";

const unknownEndpoint = (req, res) => {
    res.status(404).send({error: "Unknown endpoint!!"});
};

const errorHandler = (error, req, res, next) => {
    console.log("error ", error);
    next();
};

const logger = (req, res, next) => {
    console.log("time ", new Date(Date.now()).toString());
    console.log("method ", req.method);
    console.log("url ", req.url);
    if(req.body !== undefined){
        console.log("body ", req.body);
    }
    next();
};

const authenticate = async (req, res, next) => {
    const auth = req.get("Authorization");

    return new Promise((res, rej) => {
        if(!auth?.startsWith("Bearer ")){
            rej();
        }
        res(auth);
    }).then(auth => {
        const token = auth.substring(7);
        const secret = process.env.SECRET;

        const decodedToken = jwt.verify(token, secret);
        req.user = decodedToken;
        next();
    }).catch(error => {
        return res.status(401).send("Invalid token");
    });
};

const parseUser = (req, res, next) => {
    function getAllParams(params, body){
        return params.reduce((obj, name) => {
            const value = body[name];
            if(value === undefined || value.length === 0){
                throw `Missing ${name} param`;
            }
            obj[name] = value;
            return obj;
        }, {});
    }
    
    function getUserParams(body){
        return getAllParams(["username", "password"], body);
    }

    try{
        req.user = getUserParams(req.body);
    }catch(error){
        return res.status(401).send();
    }
    next();
};

const isAdmin = (req, res, next) => {
    req.user.isAdmin ? next() : res.status(401).send("Must be admin!");
};

export {parseUser, isAdmin, unknownEndpoint, errorHandler, logger, authenticate};