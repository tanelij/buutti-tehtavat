import argon2 from "argon2";
import express from "express";
import jwt from "jsonwebtoken";
import {parseUser} from "./middleware.js";
const router = express.Router();
const SECRET = process.env.SECRET;
const users = new Map();
router.use(express.json());


function getToken(user){
    return jwt.sign(user, SECRET);
}

function hashPassword(password){
    return argon2.hash(password);
}

function verifyPassword(hash, password){
    return argon2.verify(hash, password);
}


router.post("/register", parseUser, (req, res) => {
    const {username, password} = req.user;

    if(users.has(username)){
        throw "User already exists";
    }

    hashPassword(password).then(hash => {
        const user = {
            username,
            hash
        };
        users.set(username, user);
        const token = getToken(user);
        res.status(201).send(token);
    });
});

router.get("/login", parseUser, (req, res) => {
    const {username, password} = req.user;

    new Promise((resolve, reject) => {
        const user = users.get(username);
        if(user !== undefined){
            resolve(user);
        }else{
            reject(`No user with username: "${username}"`);
        }
    }).then(user => {
        const hash = user.hash;
        return verifyPassword(hash, password).then((matches) => {
            if(matches){
                const token = getToken(user);
                res.status(200).send(token);
            }else{
                throw "Password doesn't match";
            }
        });
    }).catch((error) => {
        res.status(401).send(error);
    });
}); 

router.post("/admin", parseUser, async (req, res) => {
    const {username, password} = req.user;
    const ADMIN_USERNAME = process.env.ADMIN_USERNAME;
    const ADMIN_PASSWORD = process.env.ADMIN_PASSWORD;
    const notAdminUsername = username !== ADMIN_USERNAME;
    const notAdminPassword = !await argon2.verify(ADMIN_PASSWORD, password);

    if(notAdminUsername || notAdminPassword){
        return res.status(401).send();
    }
    hashPassword(password).then(hash => {
        const adminObj = {
            username: ADMIN_USERNAME,
            hash: hash,
            isAdmin: true
        };
        const token = getToken(adminObj);
        res.send(token);
    });
});

export default router;