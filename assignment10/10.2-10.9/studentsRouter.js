import express from "express";
import {isAdmin, unknownEndpoint, logger, errorHandler } from "./middleware.js";
const router = express.Router();
router.use(express.json());
router.use(logger);
router.use(errorHandler);
const students = {};

router.post("/", (req, res) => {
    const [id, name, email] = ["id", "name", "email"].map(paramName => {
        if(req.body[paramName] === undefined){
            throw `Missing ${paramName} for student`;
        }
        return req.body[paramName];
    });
    students[id] = {
        id,
        name,
        email
    };
    res.status(201).send();
});

router.get("/:id", (req, res) => {
    const student = students[req.params.id];
    if(student === undefined){
        res.status(404).send();
    }
    console.log("student ", student);    
    res.json(student);
});

router.get("/", (req, res) => {
    const ids = Object.keys(students);
    
    res.json(ids);
});

router.put("/:id", isAdmin, (req, res) => {
    const user = req.user;
    if(user === undefined){
        throw "No user!";
    }
    if(!user.isAdmin){
        throw "User must be admin";
    }

    const {name, email} = req.body;
    const params = [[name, "name"], [email, "email"]]
        .filter(([value, name]) => {
            return value !== undefined && value !== "";
        });
    if(params.length === 0){
        throw "Missing params";
    }

    const student = students[req.params.id];
    if(student){
        params.forEach(([value, name]) => {
            student[name] = value;
        });

        res.status(204).send();
    }else{
        throw `No student with id: ${req.params.id}`;
    }
});

router.delete("/:id", isAdmin, (req, res) => {
    const id = req.params.id;
    const student = students[id];
    if(student){
        delete students[id];
        res.status(204).send();
    }else{
        throw `No student with id: ${req.params.id}`;
    }
});

export default router;