import express from "express";
import userRouter from "./userRouter.js";
import studentsRouter from "./studentsRouter.js";
import { authenticate, unknownEndpoint } from "./middleware.js";

const server = express();
server.use("/user", userRouter);
server.use("/students", authenticate, studentsRouter);
server.use(unknownEndpoint);

export default server;