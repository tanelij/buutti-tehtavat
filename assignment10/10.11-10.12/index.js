import server from "./server.js";

server.listen(3000, () => {
    console.log("Listening to port 3000");
});