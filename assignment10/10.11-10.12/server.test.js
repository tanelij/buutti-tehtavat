import dotenv from "dotenv";
import request from "supertest";
import server from "./server.js";
import jwt from "jsonwebtoken";
const login = "/api/v1/users/login";
const register = "/api/v1/users/register";
dotenv.config({path: "./.env"});
const secret = process.env.SECRET;

async function registerUser(body){
    return request(server)
        .post(register)
        .send(body);
}

async function loginUser(body){
    return request(server)
        .get(login)
        .send(body);
}

const validUser = {
    username: "user",
    password: "password"
};

const userWithInvalidPassword = {
    username: "user",
    password: "wordPass"
};

const nonExistantUser = {
    username: "user5",
    password: "password"
};

const admin = {
    username: process.env.ADMIN_USERNAME,
    password: process.env.ADMIN_PASSWORD
};

describe("/api/v1/users/register", () => {
    it("Returns 404 on invalid address", async () => {
        const response = await request(server)
            .post(`${register}/invalid`);
        expect(response.statusCode).toBe(404);
    });

    it("Should return 401 request body doesn't contain necessary params", async () => {
        const invalidParams = [
            {
                username: "user"
            },
            {
                password: "wordPass"
            },
            {
                random: "random"
            }
        ];
        invalidParams.forEach(async (body) => {
            const response = await registerUser(body);

            expect(response.statusCode).toBe(401);
        });
    });

    it("Should return a valid token on register", async () => {
        const response = await registerUser(validUser);
        expect(response.statusCode).toBe(201);

        const token = response.text;
        expect(() => {
            jwt.verify(token, secret);
        }).not.toThrow();
    });
});

describe("/api/v1/users/login", () => {

    it("Returns 404 on invalid address", async () => {
        const response = await request(server)
            .get(`${login}/invalid`)
            .send(validUser);
        expect(response.statusCode).toBe(404);
    });

    it("Should return 401 request body doesn't contain necessary params", async () => {
        const invalidParams = [
            {
                username: "user"
            },
            {
                password: "wordPass"
            },
            {
                random: "random"
            }
        ];
        invalidParams.forEach(async (body) => {
            const response = await request(server)
                .get(login)
                .send(body);

            expect(response.statusCode).toBe(401);
        });
    });

    it("Should return 401 on non matching password", async() => {
        registerUser(validUser);

        const response = await loginUser(userWithInvalidPassword);
        expect(response.statusCode).toBe(401);
    });

    it("Should return 401 on non existant user", async() => {
        const response = await loginUser(nonExistantUser);
        expect(response.statusCode).toBe(401);
    });

    it("Should return a valid token on login", async () => {
        registerUser(validUser);

        const response = await loginUser(validUser);
        expect(response.statusCode).toBe(200);

        const token = response.text;
        expect(() => {
            jwt.verify(token, secret);
        }).not.toThrow();
    });

    it("Admin token should include isAdmin-parameter", async () => {
        const response = await loginUser(admin);
        expect(response.statusCode).toBe(200);

        const token = response.text;
        expect(() => {
            const verifiedToken = jwt.verify(token, secret);
            expect(verifiedToken.isAdmin).toBe(true);
        }).not.toThrow();
    });
});

const bookRouterBase = "/api/v1/books/";
const book = {
    "name": "The shining",
    "author": "Stephen King",
    "read": false
};
let returnedBook;
let id;
let pathToBook;
describe(`post ${bookRouterBase}`, () => {
    async function sendRequest(body, auth){
        if(auth === undefined){
            return request(server)
                .post(bookRouterBase)
                .send(body);
        }

        return request(server)
            .post(bookRouterBase)
            .set("Authorization", `Bearer ${auth}`)
            .send(body);
    }
    it("Should fail with no auth", async () => {
        const response = await sendRequest(book);

        expect(response.statusCode).toBe(401);
    });

    it("Should fail with normal user", async () => {
        const token = await loginUser(validUser);
        const auth = token.text;

        const response = await sendRequest(book, auth);

        expect(response.statusCode).toBe(401);
    });

    it("Should succeed with admin", async () => {
        const token = await loginUser(admin);
        const auth = token.text;

        const response = await sendRequest(book, auth);
        expect(response.statusCode).toBe(201);
        expect(response.body.id).not.toBe(undefined);
        returnedBook = response.body;
        id = returnedBook.id;
        pathToBook = `${bookRouterBase}/${id}`;
    });
});

describe(`get ${bookRouterBase}`, () => {
    async function sendRequest(auth){
        if(auth === undefined){
            return request(server)
                .get(bookRouterBase);
        }

        return request(server)
            .get(bookRouterBase)
            .set("Authorization", `Bearer ${auth}`);
    }
    it("Should fail with no auth", async () => {
        const response = await sendRequest();

        expect(response.statusCode).toBe(401);
    });

    it("Should succeed with normal user", async () => {
        const token = await loginUser(validUser);
        const auth = token.text;

        const response = await sendRequest(auth);

        expect(response.statusCode).toBe(200);
    });

    it("Should succeed with admin", async () => {
        const token = await loginUser(admin);
        const auth = token.text;

        const response = await sendRequest(auth);

        expect(response.statusCode).toBe(200);
    });

    it("Response body should contain one book with proper id", async () => {
        const token = await loginUser(validUser);
        const auth = token.text;

        const response = await sendRequest(auth);

        expect(response.body).toEqual([id]);
    });
});

function getBook(auth){
    if(auth === undefined){
        return request(server)
            .get(pathToBook);
    }

    return request(server)
        .get(pathToBook)
        .set("Authorization", `Bearer ${auth}`);
}
describe(`get ${pathToBook}`, () => {
    it("Should fail with no auth", async () => {
        const response = await getBook();

        expect(response.statusCode).toBe(401);
    });

    it("Should succeed with normal user", async () => {
        const token = await loginUser(validUser);
        const auth = token.text;

        const response = await getBook(auth);

        expect(response.statusCode).toBe(200);
    });

    it("Should succeed with admin", async () => {
        const token = await loginUser(admin);
        const auth = token.text;

        const response = await getBook(auth);

        expect(response.statusCode).toBe(200);
    });

    it("Response body should contain the added book", async () => {
        const token = await loginUser(validUser);
        const auth = token.text;
        const expectedResult = {
            id,
            author: book.author,
            name: book.name,
            read: book.read
        };

        const response = await getBook(auth);

        expect(response.body).toEqual(expectedResult);
        returnedBook = response.body;
    });
});

describe(`put ${pathToBook}`, () => {
    async function sendRequest(body, auth){
        if(auth === undefined){
            return request(server)
                .put(pathToBook)
                .send(body);
        }

        return request(server)
            .put(pathToBook)
            .set("Authorization", `Bearer ${auth}`)
            .send(body);
    }
    const newBook = {
        author: "kalle"
    };
    it("Should fail with no auth", async () => {
        const response = await sendRequest(newBook);

        expect(response.statusCode).toBe(401);
    });

    it("Should fail with normal user", async () => {
        const token = await loginUser(validUser);
        const auth = token.text;

        const response = await sendRequest(newBook, auth);

        expect(response.statusCode).toBe(401);
    });

    it("Should succeed with admin", async () => {
        const token = await loginUser(admin);
        const auth = token.text;

        const response = await sendRequest(newBook, auth);

        expect(response.statusCode).toBe(204);
    });

    it("Response body should contain the modified book", async () => {
        const token = await loginUser(admin);
        const auth = token.text;
        let expectedObj =  Array.from(Object.entries(returnedBook))
            .reduce((obj, [key, value]) => {
                obj[key] = value;
                return obj;
            }, {});

        Array.from(Object.entries(newBook)).forEach(([key, value]) => {
            expectedObj[key] = value;
        });
        const response = await getBook(auth);
        expect(response.body).toEqual(expectedObj);
    });
});

