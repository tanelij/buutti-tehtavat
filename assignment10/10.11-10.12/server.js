import dotenv from "dotenv";
dotenv.config({path: "./.env"});
import express from "express";
import bookRouter from "./bookRouter.js";
import helmet from "helmet";
import { unknownEndpoint, errorHandler } from "../10.10/middleware.js";
import userRouter from "./userRouter.js";
import {authenticate} from "../10.2-10.9/middleware.js";

const server = express();
server.use(helmet());
server.use(express.json());
server.use(errorHandler);
server.use("/api/v1/books", authenticate, bookRouter);
server.use("/api/v1/users", userRouter);
server.use(express.static("public"));
server.use(unknownEndpoint);

export default server;