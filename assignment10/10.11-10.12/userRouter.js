import dotenv from "dotenv";
dotenv.config({path: "./.env"});
import argon2 from "argon2";
import express from "express";
import jwt from "jsonwebtoken";
import {parseUser} from "../10.2-10.9/middleware.js";
const router = express.Router();
const SECRET = process.env.SECRET;
const users = new Map();
router.use(express.json());
await createUser(process.env.ADMIN_USERNAME, process.env.ADMIN_PASSWORD, true);


function getToken(user){
    return jwt.sign(user, SECRET);
}

function hashPassword(password){
    return argon2.hash(password);
}

function verifyPassword(hash, password){
    return argon2.verify(hash, password);
}

async function createUser(username, password, isAdmin = false){
    if(users.has(username)){
        throw "User already exists";
    }

    const hash = await hashPassword(password);
    const user = {
        username,
        hash
    };
    if(isAdmin){
        user.isAdmin = true;
    }
    users.set(username, user);
    const token = getToken(user);
    return token;
}


router.post("/register", parseUser, async (req, res) => {
    const {username, password} = req.user;

    try{
        const token = await createUser(username, password);
        res.status(201).send(token);
    }catch(error){
        res.status(400).send();
    }
});

router.get("/login", parseUser, async (req, res) => {
    const {username, password} = req.user;

    try{
        const user = users.get(username);
        if(user === undefined){
            throw `No user with username: "${username}"`;
        }
        const hash = user.hash;
        const matches = await verifyPassword(hash, password);
        if(!matches){
            throw "Password doesn't match";
        }
        const token = getToken(user);
        res.status(200).send(token);
    }catch(error){
        res.status(401).send(error);
    }
}); 

export default router;