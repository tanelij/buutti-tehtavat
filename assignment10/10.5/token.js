import "dotenv/config";
import jwt from "jsonwebtoken";

const payload = {username: "user"};
const secret = process.env.SECRET;
const options = {expiresIn: (60*15)};

const token = jwt.sign(payload, secret, options);

console.log("Signature: ", token);

try{
    const result = jwt.verify(token, secret);
    console.log("Verified token: ", result);
}catch(error){
    console.log("Invalid token!!");
}