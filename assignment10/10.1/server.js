import express from "express";
import router from "./routers.js";

const server = express();
server.use("/students", router);
server.use(express.static("public"));

server.listen(3000, () => {
    console.log("Listening to port 3000");
});