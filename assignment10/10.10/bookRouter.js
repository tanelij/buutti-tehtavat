class Book{
    constructor(id, name, author, read){
        this.id = id;
        this.name = name;
        this.author = author;
        this.read = read;
    }

    getUpdatableParams(){
        return ["name", "author", "read"];
    }
}

const books = new Map();

function getId(){
    const largestId = Array.from(books.keys())
        .map(id => parseInt(id))
        .reduce((largest, id) => id > largest ? id : largest, 0);
    return (largestId + 1).toString();
}

import express from "express";
import { unknownEndpoint, logger, errorHandler } from "./middleware.js";
const router = express.Router();
router.use(express.json());
router.use(logger);
router.use(errorHandler);

router.get("/:id", (req, res) => {
    const book = books.get(req.params.id);
    if(book === undefined){
        return res.status(404).send();
    }
    res.json(book);
});

router.get("/", (req, res) => {
    const ids = Array.from(books.keys());
    
    res.json(ids);
});

router.post("/", (req, res) => {
    const {name, author, read} = req.body;
    const id = getId();
    const book = new Book(id, name, author, read);
    books.set(id, book);
    res.status(201).json(book);
});

router.put("/:id", (req, res) => {
    const book = books.get(req.params.id);
    if(book){
        Object.keys(book).filter(k => k !== "id")
            .forEach((key) => {
                const newValue = req.body[key];
                if(newValue === undefined || newValue.length === 0){
                    return;
                }

                book[key] = newValue;
            });

        res.status(204).send();
    }else{
        throw `No book with id: ${req.params.id}`;
    }
});

router.delete("/:id", (req, res) => {
    const id = req.params.id;
    const success = books.delete(id);
    if(success){
        res.status(204).send();
    }else{
        throw `No book with id: ${req.params.id}`;
    }
});

router.use(unknownEndpoint);

export default router;