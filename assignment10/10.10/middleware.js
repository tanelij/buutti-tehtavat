const unknownEndpoint = (req, res) => {
    res.status(404).send({error: "Unknown endpoint!!"});
};

const errorHandler = (error, req, res, next) => {
    console.log("error ", error);
    next();
};

const logger = (req, res, next) => {
    console.log("time ", new Date(Date.now()).toString());
    console.log("method ", req.method);
    console.log("url ", req.url);
    if(req.body !== undefined){
        console.log("body ", req.body);
    }
    next();
};

const validator = (req, res, next) => {
    const {name, author, read} = req.body;
    const hasAllParams = [name, author, read]
        .every(value => value !== undefined);
    if(!hasAllParams){
        res.status(400).send();
    }
    next();
};

export {validator, unknownEndpoint, errorHandler, logger};