import dotenv from "dotenv/config";
import express from "express";
import bookRouter from "./bookRouter.js";
import helmet from "helmet";
import { unknownEndpoint } from "./middleware.js";
import userRouter from "../10.2-10.9/userRouter.js";
import {authenticate} from "../10.2-10.9/middleware.js";

const server = express();
server.use(helmet());
server.use(express.json());
server.use("/api/v1/books", authenticate, bookRouter);
server.use("/api/v1/users", userRouter);
server.use(express.static("public"));
server.use(unknownEndpoint);

server.listen(3000, () => {
    console.log("Listening to port 3000");
});