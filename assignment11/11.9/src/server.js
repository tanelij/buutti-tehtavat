import express from "express";
import router from "./noticeBoard.js";

const server = express();
server.use("/notes", router);

export default server;