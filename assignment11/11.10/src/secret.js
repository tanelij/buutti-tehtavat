import dotenv from "dotenv/config";
import express from "express";
import notesRouterFactory from "./helpers.js";
import argon2 from "argon2";
const ADMIN_PASSWORD = await (
    async (password) => {
        return await argon2.hash(password);
    })(process.env.ADMIN_PASSWORD);
const ADMIN_USERNAME = process.env.ADMIN_USERNAME;

const router = express.Router();
router.use(express.json());
router.use(verifyUser, notesRouterFactory());

function getParams(body, ...params){
    return params.reduce((obj, param) => {
        const value = body[param];

        if(value === undefined || value.length === 0){
            throw `Missing param: ${param}`;
        }
        obj[param] = value;
        return obj;
    }, {});
}

async function verifyUser(req, res, next){
    try{
        const {user} = getParams(req.body, "user");
        const {username, password} = getParams(user, "username", "password");
        if(username !== ADMIN_USERNAME){
            throw "Wrong username";
        }
        const matches = await argon2.verify(ADMIN_PASSWORD, password);
        if(!matches){
            throw "Wrong password";
        }
        next();
    }catch(error){
        res.status(400).send(error);
    }
}

export default router;