import express from "express";

function getNoteRouter(){
    function addNote(text){
        const id = idCounter.toString();
        const note = {
            id,
            text
        };
        notes.set(id, note);
        idCounter++;
        return note;
    }
    
    const getNote = (req, res, next) => {
        try{
            const id = req.params.id;
            const note = notes.get(id);
            if(note === undefined){
                throw `No note with id: ${id.id}`;
            }
            req.note = note;
        }catch (error){
            res.status(404).send(error);
        }
        next();
    };

    const router = express.Router();
    router.use(express.json());

    const notes = new Map();
    let idCounter = 1;

    router.get("/", (req, res) => {
        res.status(200).json(Array.from(notes.values()));
    });

    router.get("/:id", getNote, (req, res) => {
        res.status(200).json(req.note);
    });

    router.post("/", (req, res) => {
        const {text} = req.body;

        if(text === undefined || text.length === 0){
            return res.status(400).send();
        }

        const note = addNote(text);
        res.status(200).send(note);
    });

    router.delete("/:id", getNote, (req, res) => {
        const note = req.note;
        if(!notes.delete(note.id)){
            throw "Error deleting note";
        }
        res.status(204).send();
    });

    return router;
}

export default getNoteRouter;