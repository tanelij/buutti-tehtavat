const notes = new Map();
let idCounter = 1;

function addNote(text){
    const id = idCounter.toString();
    const note = {
        id,
        text
    };
    notes.set(id, note);
    idCounter++;
    return note;
}

const getNote = (req, res, next) => {
    try{
        const id = req.params.id;
        const note = notes.get(id);
        if(note === undefined){
            throw `No note with id: ${id.id}`;
        }
        req.note = note;
    }catch (error){
        res.status(404).send(error);
    }
    next();
};

export {getNote};