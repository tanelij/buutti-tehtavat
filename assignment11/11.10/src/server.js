import express from "express";
import notesRouterFactory from "./helpers.js";
import secretRouter from "./secret.js";

const server = express();
server.use("/notes", notesRouterFactory());
server.use("/secret/notes", secretRouter);

export default server;