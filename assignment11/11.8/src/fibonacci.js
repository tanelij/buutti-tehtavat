function * fibonacci(){
    let n1 = 1;
    let n2 = 0;

    while(true){
        let n3 = n1 + n2;
        n1 = n2;
        n2 = n3;
        yield n3;
    }
}

const iterator = fibonacci();

async function printFibonacci(){
    while(true){
        const n = await new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(iterator.next().value);
            }, 1000);
        });
        console.log(n);
    }
}

printFibonacci();