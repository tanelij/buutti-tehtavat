const createPost = document.getElementById("create-post");
const nameInput = document.getElementById("name-input");
const postForm = document.getElementById("post-form");
const postArea = document.getElementById("post-area");
const submitPost = document.getElementById("submit-post");
const postSection = document.getElementById("post-section");

function addPost(name, bodyStr){
    const post = document.createElement("div");
    const headerContainer = document.createElement("div");
    const header = document.createElement("h2");
    const body = document.createElement("p");
    const deleteButton = document.createElement("button");

    headerContainer.classList.add("header-container");
    header.innerText = name;
    body.innerText = bodyStr;
    deleteButton.innerText = "Delete";
    deleteButton.onclick = () => {
        postSection.removeChild(post);
    };
    deleteButton.classList.add("delete-button");
    post.classList.add("post");

    headerContainer.appendChild(deleteButton);
    headerContainer.appendChild(header);
    post.appendChild(headerContainer);
    post.appendChild(body);
    postSection.appendChild(post);
}

createPost.onclick = () => {
    postForm.classList.toggle("hidden");
};

submitPost.onclick = () => {
    const name = nameInput.value;
    const postBody = postArea.value;

    addPost(name, postBody);
    nameInput.value = "";
    postArea.value = "";
};