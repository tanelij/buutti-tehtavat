import { getInput } from "../helpers.js";

function formatString(upperOrLower, str){
    switch(upperOrLower){
    case("lower"):
        return str.toLowerCase();
    case("upper"):
        return str.toUpperCase();
    default:
        console.log(`No option for ${upperOrLower}`);
    }
}

const [upperOrLower, str] = getInput(2);
console.log(formatString(upperOrLower, str));
