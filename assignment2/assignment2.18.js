const names = getInput(3);

console.log(formatNames(names));
const nameString = orderNames(names).join(" ");
console.log(nameString);

function getInput(n){
    return Object.values(process.argv).slice(2, 2 + n);
}

function formatNames(names){
    return names.map(name => {
        return name.slice(0,1);
    }).reduce((prev, cur) => {
        return prev + "." + cur;
    });
}

function orderNames(names){
    return names.sort((a, b) => {
        return b.length - a.length;
    });
}
