const arr = [1, 4, 6, 32, 25, 16, 31, 15, 10, 2, 7];

let largest = arr.reduce((prev, cur) => {
    return cur > prev ? cur : prev;
});
console.log(largest);

function extra(){
    let secondLargest = Number.NEGATIVE_INFINITY;
    arr.reduce((largest, number) => {
        if(number < largest && number > secondLargest){
            secondLargest = number;
        }else if(number > largest){
            secondLargest = largest;
        }
        return number > largest ? number : largest;
    });
    console.log(secondLargest);
}

extra();