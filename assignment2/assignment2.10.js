const students = [
    { name: "Markku", score: 99 },
    { name: "Karoliina", score: 58 },
    { name: "Susanna", score: 69 },
    { name: "Benjamin", score: 77 },
    { name: "Isak", score: 49 },
    { name: "Liisa", score: 89 },
];

const highest = students.reduce((prev, cur) => {
    return cur.score > prev.score ? cur : prev;
});

const lowest = students.reduce((prev, cur) => {
    return cur.score < prev.score ? cur : prev;
});

const average = students.map((s) => s.score)
    .reduce((prev, cur) => {
        return cur + prev;
    }) / students.length;

const higherThanAverage = students.reduce((arr, cur) => {
    if(cur.score > average){
        arr.push(cur);
    }
    return arr;
}, []).map(s => s.name);

const lowerThanAverage = students.reduce((arr, cur) => {
    if(cur.score < average){
        arr.push(cur);
    }
    return arr;
}, []).map(s => s.name);

const grades = students.map((s) => {
    const grade = Math.round((s.score / highest.score) * 5);
    return `${s.name} grade: ${grade}`;
});

console.log(`highest: ${highest.score}
lowest: ${lowest.score}
average: ${average}
higher than average: ${higherThanAverage}
lower than average: ${lowerThanAverage}
grades ${grades}`);
