const n = 17;

function sumViaFor(){
    let sum = 0;
    for(let i=1; i<=n; i++){
        if(addNumber(i)){
            sum += i;
        }
    }
    console.log(sum);
}

function sumViaWhile(){
    let sum = 0;
    let i = 0;
    while(i<=n){
        if(addNumber(i)){
            sum += i;
        }
        i++;
    }
    console.log(sum);
}

function addNumber(number){
    const divisibleBy3 = number % 3 === 0;
    const divisibleBy5 = number % 5 === 0;

    return divisibleBy3 || divisibleBy5;
}

sumViaFor();
sumViaWhile();