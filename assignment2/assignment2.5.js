const n = 4;

function sumViaFor(){
    let sum = 0;
    for(let i=1; i<=n; i++){
        sum += i;
    }
    console.log(sum);
}

function sumViaWhile(){
    let sum = 0;
    let i = 0;
    while(i<=n){
        sum += i;
        i++;
    }
    console.log(sum);
}

sumViaFor();
sumViaWhile();