import {getInput} from "../helpers.js";

function removeLastWord(str){
    return str.replace(/\w+\W*$/, "");
}

function replaceChar(str, chr, replacement){
    return str.replaceAll(chr, replacement);
}

let [chr, replacementChr, str] = getInput(3);
console.log(getInput(3));

if(str === undefined){
    str = chr;
    console.log(removeLastWord(str));
}else{
    console.log(replaceChar(str, chr, replacementChr));
}