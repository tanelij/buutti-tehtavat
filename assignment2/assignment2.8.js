function level1(height){
    for(let i=0; i<height; i++){
        let str = "";
        for(let j=0; j<i; j++){
            str += "&";
        }
        console.log(str);
    }
}

function level2(height){
    for(let i=0; i<height; i++){
        const amountOfSymbols = 1 + (i*2);
        const rowsBelow = height - (i+1);
        const amountOfSpaces = rowsBelow;
        let str = "";

        for(let j=0; j < amountOfSpaces; j++){
            str += " ";
        }
        for(let j=0; j<amountOfSymbols; j++){
            str += "&";
        }
        console.log(str);
    }
}

function level3(height){
    let i = 0;
    while(i<height){
        const amountOfSymbols = 1 + (i*2);
        const rowsBelow = height - (i+1);
        const amountOfSpaces = rowsBelow;
        let str = "";

        let j=0;
        while(j<amountOfSpaces){
            str += " ";
            j++;
        }
        j=0;
        while(j<amountOfSymbols){
            str += "&";
            j++;
        }
        console.log(str);
        i++;
    }
}

level1(4);
console.log();
level2(4);
console.log();
level3(4);