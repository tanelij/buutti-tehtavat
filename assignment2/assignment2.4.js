printStr((str) => {
    for(let i=100; i<=1000; i+=100){
        str += i + " ";
    }
    return str;
});

printStr((str) => {
    for(let i=1; i<=128; i+=i){
        str += i + " ";
    }
    return str;
});

printStr((str) => {
    for(let i=3; i<=15; i+=3){
        str += i + " ";
    }
    return str;
});

printStr((str) => {
    for(let i=9; i>=0; i--){
        str += i + " ";
    }
    return str;
});

printStr((str) => {
    for(let i=1; i<=4; i++){
        for(let j=0; j<3; j++){
            str += i + " ";
        }
    }
    return str;
});

printStr((str) => {
    for(let i=1; i<=4; i++){
        for(let j=0; j<=4; j++){
            str += j + " ";
        }
    }
    
    return str;
});


function printStr(fnc){
    let str = "";
    str = fnc(str);
    console.log(str);
    console.log();
}
