import pg from "pg";

const pool = new pg.Pool({
    "host": "localhost",
    "user": "pguser",
    "password": "pgpass",
    "port": 5432,
    "database": "songs",
    "ssl": false
});

async function executeQuery(query, ...params){
    const client = await pool.connect();
    try{
        const result = await client.query(query, params);
        return result;
    }catch(error){
        error.type = "db error";
        throw error;
    }finally{
        client.release();
    }
}

export default executeQuery;