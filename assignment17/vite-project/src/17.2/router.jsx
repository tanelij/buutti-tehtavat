import { createBrowserRouter, RouterProvider, useLoaderData } from "react-router-dom";
import {loader} from "./contacts"

function Page(){
    const {name, phone, email} = useLoaderData();
    return (
        <ul>
            <li><i>name: </i>{name}</li>
            <li><i>phone: </i>{phone}</li>
            <li><i>email: </i>{email}</li>
        </ul>
    )
}

const router = createBrowserRouter([
    {
        path: "contact/:id",
        element: <Page></Page>,
        loader
    }
])
const Component = () => {

    return (
        <RouterProvider router={router}></RouterProvider>
    )
}

export default Component;