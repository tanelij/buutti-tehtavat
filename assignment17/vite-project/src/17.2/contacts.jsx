class Contact{
    constructor(id, name, phone, email){
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
    }
    toString(){
        return Object.entries(this).map(([key, value]) => {
            return `${key}: ${value}`;
        }).join("\n");
    }

    toElem(){
        const listItems = Object.entries(this).map(([key, value]) => {
            return <li key={key}><i>{key}: </i>{value}</li>;
        })
        return (
            <ul>{listItems}</ul>
        )
    }
}

const contacts = [
    ["Jorma", "050-3657741", "jorma@gmail.com"],
    ["Kalle", "050-3327711", "Kalle@gmail.com"],
    ["Seppo", "040-2257681", "Seppo@gmail.com"]
].map((arr, i) => {
    return new Contact(i.toString(), ...arr)
})

export function loader({params}){
    const contact = contacts.find(c => c.id === params.id);
    return contact;
}