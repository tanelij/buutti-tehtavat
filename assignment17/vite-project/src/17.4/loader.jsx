import contacts from "./contacts.js";

function loader({params}){
    const contact = contacts.find(c => c.id === params.id);
    if(contact === undefined){
        const error = new Error();
        error.status = 404;
        throw error;
    }
    return contact;
}

export {
    loader
};