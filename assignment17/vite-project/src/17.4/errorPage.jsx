import {useRouteError, Link} from "react-router-dom";

export default function ErrorPage(){
    const error = useRouteError();

    switch(error.status){
        case(404):
            return (
                <div>
                    <h1>404 - Not Found</h1>
                    <p>The requested resource is not here!</p>
                    <Link to="/">Main Page</Link>
                </div>
            )
        default:
            return (
                <div>
                    <h1>An Unexpected error happened!</h1>
                    <p>
                        {error.statusText || error.message}
                    </p>
                </div>
            )
    }
}