import { createBrowserRouter, RouterProvider, useLoaderData } from "react-router-dom";
import {Link, Outlet} from "react-router-dom";
import {loader} from "./loader.jsx";
import contacts from "./contacts.js";
import ErrorPage from "./errorPage.jsx";
import "./style.css";

function Page(){
    const {name, phone, email} = useLoaderData();
    return (
        <ul>
            <li><i>name: </i>{name}</li>
            <li><i>phone: </i>{phone}</li>
            <li><i>email: </i>{email}</li>
        </ul>
    )
}

const App = () => {
    const links = contacts.map(({id, name}) => {
        return (
            <Link className="nav-link" key={id} to={`/contacts/${id}`}>{name}</Link>
        )
    })
    return (
        <div>
            <nav className="nav">
                {links}
            </nav>
            <Outlet></Outlet>
        </div>
    )
}

const Component = () => {
    return (
        <RouterProvider router={router}>
        </RouterProvider>
    )
}

const AdminPage = () => {
    return (
        <div>This is the admin page!</div>
    )
}

const router = createBrowserRouter([
    {
        path: "/",
        element: <App></App>,
        errorElement: <ErrorPage></ErrorPage>,
        children: [
            {
                path: "contacts/:id",
                element: <Page></Page>,
                loader
            },
            {
                path: "admin",
                element: <AdminPage></AdminPage>
            }
        ]
    }
])


export default Component;