import PropTypes from "prop-types";
import { useState } from "react";

const List = ({name, elements, decoration}) => {
    function getDecoratedContent(text){
        switch(decoration){
            case("bold"):
                return <b>{text}</b>
            case("italic"):
                return <i>{text}</i>
            case("both"):
                return <b><i>{text}</i></b>
            case("none"):
                return text
            default:
                return text
        }
    }

    const listItems = elements.map((str, i) => {
        return (
            <li key={i}>{getDecoratedContent(str)}</li>
        )
    })

    return (
        <div>
            <h2>{getDecoratedContent(name)}</h2>
            <ul>{listItems}</ul>
        </div>
    )
}

const InputWithLabel = ({id, setter, value}) => {
    const text = useState("");

    const onChange = (e) => {
        setter(e.target.value);
    }
    return (
        <>
            <label htmlFor={id}>{id}</label>
            <br></br>
            <input value={value} onChange={onChange} type="text" name="" id={id} />
            <br></br>
        </>
    )
}

const RadioGroup = ({name, setter, value}) => {
    return (
        <input type="radio" name={name} id="" />
    )
}

List.propTypes = {
    name: PropTypes.string.isRequired,
    elements: PropTypes.arrayOf(PropTypes.string).isRequired,
    decoration: PropTypes.oneOf([
        "bold", "italic", "both", "none"
    ])
};

const Form = ({setProps}) => {
    const [decoration, setDecoration] = useState("");
    const [name, setName] = useState("");
    const elements = [
        "element1", "element2", "element3"
    ]

    const handleRenderClick = () => {
        const props = Object.entries({name, decoration, elements}).reduce((obj, [key, value]) => {
            if(value.length > 0){
                obj[key] = value;
            }
            return obj;
        }, {})
        setProps(props);
    }

    return (
        <div>
            <InputWithLabel value={name} id="name" setter={setName}></InputWithLabel>
            <InputWithLabel value={decoration} id="decoration" setter={setDecoration}></InputWithLabel>
            <button onClick={handleRenderClick}>RenderList</button>
        </div>
    )
}

const Container = () => {
    const [props, setter] = useState();
    const propSetter = (props) => {
        setter(props)
    }
    return (
        <div>
            <Form setProps={propSetter}></Form>
            {
                props !== undefined 
                    ? <List {...props}></List>
                    : undefined
            }
        </div>
    )
}

export default Container;