class Contact{
    constructor(id, name, phone, email){
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
    }
    toString(){
        return Object.entries(this).map(([key, value]) => {
            return `${key}: ${value}`;
        }).join("\n");
    }
}

const contacts = [
    ["Jorma", "050-3657741", "jorma@gmail.com"],
    ["Kalle", "050-3327711", "Kalle@gmail.com"],
    ["Seppo", "040-2257681", "Seppo@gmail.com"]
].map((arr, i) => {
    return new Contact(i.toString(), ...arr);
});

export default contacts;