import { createBrowserRouter, RouterProvider, useLoaderData } from "react-router-dom";
import {Link, Outlet} from "react-router-dom";
import {loader} from "./loader.jsx";
import contacts from "./contacts.js";
import "./style.css";

function Page(){
    const {name, phone, email} = useLoaderData();
    return (
        <ul>
            <li><i>name: </i>{name}</li>
            <li><i>phone: </i>{phone}</li>
            <li><i>email: </i>{email}</li>
        </ul>
    )
}

const App = () => {
    const links = contacts.map(({id, name}) => {
        return (
            <Link className="nav-link" key={id} to={`/contacts/${id}`}>{name}</Link>
        )
    })
    return (
        <div>
            <nav className="nav">
                {links}
            </nav>
            <Outlet></Outlet>
        </div>
    )
}

const Component = () => {
    return (
        <RouterProvider router={router}>
        </RouterProvider>
    )
}

const router = createBrowserRouter([
    {
        path: "/",
        element: <App></App>,
        children: [
            {
                path: "contacts/:id",
                element: <Page></Page>,
                loader
            }
        ]
    }
])


export default Component;