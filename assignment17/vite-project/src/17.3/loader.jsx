import contacts from "./contacts.js";

function loader({params}){
    const contact = contacts.find(c => c.id === params.id);
    return contact;
}

export {
    loader
};