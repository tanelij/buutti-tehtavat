import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './17.7/songBook.jsx'


ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App></App>
  </React.StrictMode>,
)
