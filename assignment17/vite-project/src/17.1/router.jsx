import {createBrowserRouter, RouterProvider} from "react-router-dom";

const router = createBrowserRouter([
    {
        path: "contacts",
        element: <div>Contacts page</div>
    },
    {
        path: "admin",
        element: <div>Admin page</div>
    }
  ]);

const Component = () => {
    return (
        <RouterProvider router={router}></RouterProvider>
    )
}

export default Component;