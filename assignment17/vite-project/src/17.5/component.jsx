import { useState } from "react";

const Component = () => {
    const [color, setColor] = useState(randomColor())
    function randomColor(){
        function randomValue(){
            return Math.random() * 255;
        }
        const colorStr = `RGB(${randomValue()}, ${randomValue()}, ${randomValue()})`
        return colorStr;
    }
    const handleClick = () => {
        setColor(randomColor());
    }
    return (
        <div onClick={handleClick} style={{width: "30px", height:"30px", backgroundColor: color}}></div>
    )
}

export default Component;