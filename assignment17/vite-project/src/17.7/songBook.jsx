import {useState} from "react";
import { createBrowserRouter, RouterProvider, useLoaderData } from "react-router-dom";
import {Link, Outlet, NavLink} from "react-router-dom";
import ErrorPage from "./errorPage.jsx";
import songs from "./songs.js";
import "./style.css";


const songLoader = ({params}) => {
    const song = songs.find(s => s.id == params.id);
    if(song === undefined){
        const error = new Error();
        error.status = 404;
        throw error;
    }
    return song;
}

const App = () => {
    const [searchStr, setSearchStr] = useState("");
    const handleSearchChange = (e) => {
        setSearchStr(e.target.value);
    }
    const songLinks = songs.slice(0, 5)
    .filter(({title}) => title.toLowerCase().includes(searchStr.toLowerCase()))
    .map(({id, title}) => {
        return (
            <NavLink className={({isActive, isPending}) => {
                return "nav-link" + (
                    isActive 
                    ? " active"
                    : isPending
                    ? " pending"
                    : ""
                )
            }} to={`/${id}`} key={id}>{title}</NavLink>
        )
    });
    return (
        <div>
            <nav className="nav">
                <div className="song-container">{songLinks}</div>
                <input placeholder="Filter songs" className="search-input" value={searchStr} type="text" onChange={handleSearchChange}></input>
            </nav>
            <Outlet></Outlet>
        </div>
    )
}

const Songs = () => {
    const song = useLoaderData();

    const listElements = Object.entries(song)
    .filter(([key, value]) => key !== "id")
    .map(([key, value]) => {
        return (
            <li key={key}>{`${key}: ${value}`}</li>
        );
    });

    return (
        <ul>
            {listElements}
        </ul>
    )
}

const router = createBrowserRouter([
    {
        element: <App></App>,
        path: "/",
        errorElement: <ErrorPage></ErrorPage>,
        children: [
            {
                element: <Songs></Songs>,
                path: ":id",
                loader:  songLoader
            }
        ]
    }
]);

export default () => {
    return (
        <RouterProvider router={router}>
        </RouterProvider>
    )
}