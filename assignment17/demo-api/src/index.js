import express from "express";
import pg from "pg";

const client = new pg.Client({
    host: "jsc-lecture-17-demo.postgres.database.azure.com",
    port: 5432,
    database: "postgres",
    user: "jscadmin@jsc-lecture-17-demo",
    password: "jscpass1!",
    ssl: true
});
const server = express();

server.get("/messages", async (req, res) => {
    await client.connect();
    const result = await client.query("SELECT * from messages;");
    await client.end();
    res.json(result.rows);
});

server.listen(3000, () => {
    console.log("Server listening on port 3000");
});