function calcBAC(gender, weight, drinkTime, drinkSize, alcVolume, dosage){
    const litres = drinkSize * dosage;
    const grams = litres * 8 * alcVolume;
    const burning = weight / 10;
    const gramsLeft = grams - (burning * drinkTime);

    console.log("values ", [gender, weight, drinkTime, drinkSize, alcVolume, dosage])
    console.log("gramsLeft: ", gramsLeft);

    switch (gender) {
    case "male":
        return gramsLeft / (weight * 0.7);
    case "female":
        return gramsLeft / (weight * 0.6);
    default:
        throw "Invalid gender";
    }
}

const bacForm = document.getElementById("BAC");
const resultField = document.getElementById("BAC-result");

bacForm.addEventListener(
    "submit",
    (event) => {
        const data = new FormData(bacForm);
        const ids = [
            "gender",
            "weight",
            "drink-time",
            "drink-size",
            "alc-volume",
            "dosage"
        ];
        const values = ids.map(id => {
            const value = id === "gender" 
                ? data.get(id)
                : parseFloat(data.get(id));

            return id === "drink-size" 
                ? value / 1000
                : value;
        });
        const bac = Math.round(calcBAC(...values) * 100) / 100;
        resultField.innerText = `Your BAC is ${bac}`;
        console.log(bac);
        event.preventDefault();
    },
    false
);