const screen = document.getElementById("screen");
function appendInputHandler(elements){
    elements.forEach(elem => {
        elem.addEventListener("click", () => {
            setInput(getInput() + elem.innerHTML);
        });
    });
}

function getNextStateHandler(stateName){
    return (elements) => {
        elements.forEach(elem => {
            elem.addEventListener("click", () => {
                states.setState(stateName);
            });
        });
    };
}

function clearInputHandler(elements){
    elements.forEach(elem => {
        elem.addEventListener("click", () => {
            clearInput();
        });
    });
}

function calculateInputHandler(elements){
    elements.forEach(elem => {
        elem.addEventListener("click", () => {
            const input = getInput();
            let operands = input.split(/[*\-+/]/);
            const operators = input.split(/[0-9]+(?:\.?[0-9]+)?/).filter(element => element !== "");
            let value = 0;
            while(operators.length > 0){
                const [operand1, operand2] = operands.splice(0, 2).map(str => {
                    return parseFloat(str);
                });
                const operator = operators.splice(0, 1)[0];
                value = calculator(operator, operand1, operand2);
                operands = [value, ...operands];
            }
            setInput(value);
        });
    });
}

const states = {
    "elements": [
        [
            "operators", 
            ["plus", "minus", "multiply", "divide"],
            [
                getNextStateHandler("operator-given"),
                appendInputHandler
            ]
        ],
        [
            "numbers", 
            "1,2,3,4,5,6,7,8,9,0".split(","),
            [
                getNextStateHandler("operand-given"),
                appendInputHandler
            ]
        ],
        [
            "equals", 
            "equals",
            [
                getNextStateHandler("operand-given"),
                calculateInputHandler
            ]
        ],
        [
            "dot", 
            "dot",
            [
                getNextStateHandler("dot-given"),
                appendInputHandler
            ]
        ],
        [
            "c",
            "c",
            [
                getNextStateHandler("no-operand"),
                clearInputHandler
            ]
        ]
    ].reduce((obj, [key, ids, handlers]) => {
        let idArr = Array.isArray(ids) ? ids : [ids];

        const elems = idArr.map(id => {
            return document.getElementById(id);
        });

        handlers.forEach(handler => {
            handler(elems);
        });

        obj[key] = elems;
        return obj;
    }, {}),
    "states": [
        [
            "no-operand",
            [
                ["operators", false],
                ["numbers", true],
                ["dot", false],
                ["equals", false]
            ]
        ],
        [
            "operand-given",
            [
                ["operators", true],
                ["numbers", true],
                ["dot", true],
                ["equals", true]
            ]
        ],
        [
            "operator-given",
            [
                ["operators", false],
                ["numbers", true],
                ["dot", false],
                ["equals", false]
            ]
        ],
        [
            "dot-given",
            [
                ["operators", false],
                ["numbers", true],
                ["dot", false],
                ["equals", false]
            ]
        ],
    ].reduce((obj, [stateName, stateArr]) => {
        obj[stateName] = stateArr.reduce((innerObj, [elemName, isActive]) => {
            innerObj[elemName] = isActive;
            return innerObj;
        }, {});

        return obj;
    }, {}),
    setState(stateName){
        const nextState = this.states[stateName];
        for(const key in nextState){
            const active = nextState[key];
            const elements = this.elements[key];
            elements.forEach(element => {
                if(active){
                    element.removeAttribute("disabled");
                }else{
                    element.setAttribute("disabled", true);
                }
            });
        }
    }
};

function clearInput(){
    screen.innerText = "";
}

function getInput(){
    return screen.innerText;
}

function setInput(value){
    screen.innerText = value;
}
