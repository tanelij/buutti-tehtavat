const getValue = function () {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve({ value: Math.random() });
        }, Math.random() * 1500);
    });
};


async function helper(){
    let value1 = await getValue();
    let value2 = await getValue();

    console.log(value1);
    console.log(value2);
}

//helper();

function* getValues(n){
    for(let i=0; i<n; i++){
        yield getValue();
    }
}

Promise.all(getValues(2)).then(values => {
    values.forEach(({value}, i) => {
        console.log(`Value${i} = ${value}`);
    });
});


// Promise.all([getValue(), getValue()]).then(([value1, value2]) => {
//     console.log(value1);
//     console.log(value2);
// });