function waitForSeconds(n){
    helper(1);
    function helper(time){
        setTimeout(() => {
            console.log(`Wait ${time} seconds.`);
            if(time < n){
                helper(time+1);
            }
        }, 1000);
    }
}

waitForSeconds(3);