function* a(promise){
    for(let i=0; i<3; i++){
        console.log(promise);
        console.log(promise === lastPromise);
        yield promise.then(printTime);
        const lastPromise = promise;
    }
}

function printTime(time){
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log(time);
            resolve(time+1);
        }, 1000);
    });
}
Promise.all(Array.from(a(printTime(1))));
