const movieYear = document.getElementById("movie-year");
const movieName = document.getElementById("movie-name");
const movieSearch = document.getElementById("movie-search");
const output = document.getElementById("output");

function getMovieData(name, year){
    const data = fetch(`http://www.omdbapi.com/?apikey=c3a0092f&s=${name}&y=${year}`);
    data.then((result) => {
        return result.json();
    }).then((json) => {
        console.log(json);
        //output.innerText = json.toJSON();
        json.Search.forEach(result => {
            createSearchResult(result);
        });
    });

}

function createSearchResult(json){
    const relevantData = ["Title", "Year"]
        .map((key) => {
            return [key, json[key]];
        });
    const ul = document.createElement("ul");
    relevantData.forEach(([k, v]) => {
        const li = document.createElement("li");
        li.innerText = `${k}: ${v}`;
        ul.appendChild(li);
    });
    ul.appendChild(createLink(json.imdbID));
    ul.appendChild(createPoster(json.Poster));
    output.appendChild(ul);
} 

function createLink(imdbID){
    const link = `https://www.imdb.com/title/${imdbID}`;
    const a = document.createElement("a");
    a.href = link;
    a.innerText = "imdb";
    const li = document.createElement("li");
    li.appendChild(a);
    return li;
}

function createPoster(url){
    const img = document.createElement("img");
    img.src = url;
    const li = document.createElement("li");
    li.appendChild(img);
    return li;
}

function clearOutput(){
    output.innerHTML = "";
}

movieSearch.onclick = () => {
    clearOutput();
    const year = movieYear.value;
    const name = movieName.value;
    getMovieData(name, year);
};

