import axios from "axios";

async function getData(){
    const todoResult = await axios.get("https://jsonplaceholder.typicode.com/todos/");
    const todoData = todoResult.data;
    console.log(todoData.slice(0,2));
    for(let i=0; i<todoData.length; i++){
        const todo = todoData[i];
        const userResult = await axios.get(`https://jsonplaceholder.typicode.com/users/${todo.userId}`);
        todo.user = ["name", "username", "email"]
            .reduce((obj, key) => {
                obj[key] = userResult.data[key];
                return obj;
            }, {});
    }
    console.log(todoData.slice(0,2));
}

async function getData2(){
    const todoResult = await axios.get("https://jsonplaceholder.typicode.com/todos/");
    const todoData = todoResult.data;
    console.log(todoData.slice(0,2));

    const userResult = await axios.get("https://jsonplaceholder.typicode.com/users/");
    const userData = userResult.data;

    todoData.forEach(todo => {
        const user = userData.find((data) => data.id === todo.userId);
        todo.user = ["name", "username", "email"]
            .reduce((obj, key) => {
                obj[key] = user[key];
                return obj;
            }, {});
    });
    console.log(todoData.slice(0,2));
}

getData();
getData2();