import express from "express";

const server = express();

server.get("/", (_req, res) => {
    res.send("Hello world1!");
}); 

server.get("/endpoint2", (_req, res) => {
    res.send("Hello world2!");
}); 

server.listen(3000, () => {
    console.log("Listening to port 3000");
});