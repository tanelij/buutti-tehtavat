import express from "express";
import { unknownEndpoint, logger, errorHandler } from "./middleware.js";
const students = {};
const server = express();
// server.use(express.urlencoded({extended: false}));
server.use(express.json());
server.use(logger);
server.use(errorHandler);

server.use(express.static("public"));

server.post("/student", (req, res) => {
    const [id, name, email] = ["id", "name", "email"].map(paramName => {
        if(req.body[paramName] === undefined){
            throw `Missing ${paramName} for student`;
        }
        return req.body[paramName];
    });
    students[id] = {
        id,
        name,
        email
    };
    res.status(201).send();
});

server.get("/student/:id", (req, res) => {
    if(students[req.params.id] === undefined){
        res.status(404).send();
    }
    const student = students[req.params.id];
    console.log("student ", student);    
    res.json(student);
});

server.get("/students", (req, res) => {
    const ids = Object.keys(students);
    
    res.json(ids);
});

server.put("/student/:id", (req, res) => {
    const {name, email} = req.body;
    const params = [[name, "name"], [email, "email"]]
        .filter(([value, name]) => {
            return value !== undefined && value !== "";
        });
    if(params.length === 0){
        throw "Missing params";
    }

    const student = students[req.params.id];
    if(student){
        params.forEach(([value, name]) => {
            student[name] = value;
        });

        res.status(204).send();
    }else{
        throw `No student with id: ${req.params.id}`;
    }
});

server.delete("/student/:id", (req, res) => {
    const id = req.params.id;
    const student = students[id];
    if(student){
        delete students[id];
        res.status(204).send();
    }else{
        throw `No student with id: ${req.params.id}`;
    }
});

server.listen(3000, () => {
    console.log("Listening to port 3000");
});