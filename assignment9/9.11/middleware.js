const unknownEndpoint = (req, res) => {
    res.status(404).send({error: "Unknown endpoint!!"});
};

const errorHandler = (error, req, res, next) => {
    console.log("error ", error);
    next();
};

const logger = (req, res, next) => {
    console.log("time ", new Date(Date.now()).toString());
    console.log("method ", req.method);
    console.log("url ", req.url);
    if(req.body !== undefined){
        console.log("body ", req.body);
    }
    next();
};

const parseNumbers = (req, res, next) => {
    const nums = req.query.numbers?.split(",")
        .map(num => {
            const parsedNum = parseFloat(num);
            if(isNaN(parsedNum)){
                throw "Non number in query";
            }
            return parsedNum;
        });

    if(nums === undefined || nums.length === 0){
        res.send(0);
    }
    res.numbers = nums;
    next();
};

export {unknownEndpoint, errorHandler, logger, parseNumbers};