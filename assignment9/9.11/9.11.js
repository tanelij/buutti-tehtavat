import express from "express";
import {parseNumbers} from "./middleware.js";
const server = express();

server.use(parseNumbers);

server.use((error, req, res, next) => {
    res.status(404).send({error});
    next();
});

function createEndPoint(name, func){
    server.get(`/${name}`, (req, res) => {
        const result = res.numbers.reduce((result, num, i, arr) => {
            return func(result, num, arr);
        });
        res.send(result.toString());
    });
}

[
    ["substract", (n1, n2) => n1-n2],
    ["add", (n1, n2) => n1+n2],
    ["multiply", (n1, n2) => n1*n2],
    ["divide", (n1, n2, arr) => {
        if(arr.indexOf(0) > 0){
            throw "Cannot divide by zero";
        }
        return n1 / n2;
    }]
].forEach(([name, func]) => {
    createEndPoint(name, func);
});

server.listen(3000, () => {
    console.log("Listening to port 3000");
});