const unknownEndpoint = (req, res) => {
    res.status(404).send({error: "Unknown endpoint"});
};

const errorHandler = (error, req, res, next) => {
    console.log("error ", error);
    res.status(400).send({error: error});
    next(error);
};

const logger = (req, res, next) => {
    console.log("time ", new Date(Date.now()).toString());
    console.log("method ", req.method);
    console.log("url ", req.url);
    if(req.body !== undefined){
        console.log("body ", req.body);
    }
    next();
};

export {unknownEndpoint, errorHandler, logger};