import express from "express";
import { unknownEndpoint, logger } from "./middleware.js";

const server = express();
// server.use(express.urlencoded({extended: false}));
// server.use(express.json());
server.use(logger);
server.use(unknownEndpoint);


server.get("/students", (request, response) => {
    response.send([]);
});


server.listen(3000, () => {
    console.log("Listening to port 3000");
});