import http from "http";

const server = http.createServer((req, res) => {
    res.write("Hello world!");
    res.end();
});

server.listen(80);

console.log("Listening port 80");