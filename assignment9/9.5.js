import express from "express";

const server = express();

const logger = (req, res, next) => {
    console.log("time ", new Date(Date.now()).toString());
    console.log("method ", req.method);
    console.log("url ", req.baseUrl);
    next();
};

server.use("/students", logger);

server.get("/students", (request, response) => {
    response.send([]);
});

server.listen(3000, () => {
    console.log("Listening to port 3000");
});