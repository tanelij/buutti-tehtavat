import express from "express";
import { unknownEndpoint, logger, errorHandler } from "./middleware.js";
const students = {};
const server = express();
// server.use(express.urlencoded({extended: false}));
server.use(express.json());
server.use(logger);
server.use(errorHandler);



server.post("/student", (req, res) => {
    const [id, name, email] = ["id", "name", "email"].map(paramName => {
        if(req.body[paramName] === undefined){
            throw `Missing ${paramName} for student`;
        }
        return req.body[paramName];
    });
    students[id] = {
        id,
        name,
        email
    };
    res.status(201).send();
});

server.get("/student/:id", (req, res) => {
    if(students[req.params.id] === undefined){
        res.status(404).send();
    }
    const student = students[req.params.id];
    console.log("student ", student);    
    res.json(student);
});

server.get("/students", (req, res) => {
    const ids = Object.keys(students);
    
    res.json(ids);
});

server.listen(3000, () => {
    console.log("Listening to port 3000");
});