import express from "express";
import { unknownEndpoint } from "./middleware";

const server = express();

server.get("/students", (request, response) => {
    response.send([]);
});

server.use(unknownEndpoint);

server.listen(3000, () => {
    console.log("Listening to port 3000");
});