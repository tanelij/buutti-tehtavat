const unknownEndpoint = (req, res) => {
    res.status(404).send({error: "Unknown endpoint"});
};

const errorHandler = (error, req, res, next) => {
    console.log("error ", error);
    next();
};

export {unknownEndpoint, errorHandler};