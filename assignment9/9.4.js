import express from "express";
const server = express();
let counter = {};

function getCount(name){
    if(counter[name] === undefined){
        counter[name] = 1;
    }
    return counter[name];
}

function setCount(name, count){
    const countNumber = typeof(count) === "string" ? parseInt(count) : count;
    counter[name] = countNumber;
}

function incrementCounter(name){
    setCount(name, getCount(name) + 1);
}

server.get("/counter/:name", (req, res) => {
    console.log(req.query);
    const number = req.query.number;
    const name = req.params.name;
    if(number){
        setCount(name, number);
    }
    const count = getCount(name);
    const html = `<h1>${name} was here ${count} times!</h1>`;
    res.send(html);
    incrementCounter(name);
}); 

server.listen(3000, () => {
    console.log("Listening to port 3000");
});