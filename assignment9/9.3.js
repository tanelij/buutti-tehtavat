import express from "express";
const server = express();
let counter = 1;

server.get("/counter", (req, res) => {
    console.log(req.query);
    const number = req.query.number;
    if(number){
        counter = number;
    }
    const html = `<h1>Endpoint has been accessed ${counter} times!</h1>`;
    res.send(html);
    counter++;
}); 

server.listen(3000, () => {
    console.log("Listening to port 3000");
});