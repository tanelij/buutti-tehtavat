import express from "express";
import router from "./routers.js";
import helmet from "helmet";
import { unknownEndpoint } from "./middleware.js";

const server = express();
server.use(helmet());
server.use(express.json());
server.use("/api/v1/books", router);
server.use(express.static("public"));
server.use(unknownEndpoint);

server.listen(3000, () => {
    console.log("Listening to port 3000");
});