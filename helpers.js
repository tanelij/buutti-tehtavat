function getInput(n){
    return Object.values(process.argv).slice(2, 2 + n);
}

export {getInput};