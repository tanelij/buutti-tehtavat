import { useState, useEffect } from "react";

const CatInput = ({getCat}) => {
    const [catText, setCatText] = useState("");
    const handleSubmit = (e) => {
        e.preventDefault();
        getCat(catText);
    }
    const handleInputChange = (e) => {
        setCatText(e.target.value);
    }

    return (
        <form onSubmit={handleSubmit}>
            <label htmlFor={"cat-text"}>Text for the cat photo</label>
            <br></br>
            <input onChange={handleInputChange} id={"cat-text"} type="text" value={catText}></input>
            <input type="submit" value="Get cat!"></input>
        </form>
    )
}

const Component = () => {
    const [src, setSrc] = useState("https://cataas.com/cat");
    const [text, setText] = useState();
    let objUrl;

    const handleLoad = () => {
        if(objUrl !== undefined){
            URL.revokeObjectURL(objUrl);
            objUrl = undefined;
        }
    }

    useEffect(() => {
        if(text === undefined){
            return;
        }
        (async () => {
            const url = `https://cataas.com/cat/says/${text}`;
            const result = await fetch(url);
            const blob = await result.blob();
            objUrl = URL.createObjectURL(blob);
            setSrc(objUrl);
        })();
    }, [text]);

    function getCat(text){
        setText(text);
        //setSrc(`https://cataas.com/cat/says/${text}`);
    }



    return (
        <div>
            <CatInput getCat={getCat}></CatInput>
            <img height="400px" onLoad={handleLoad} src={src}></img>
        </div>
    )
};

export default Component;