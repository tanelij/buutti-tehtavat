import { useState } from "react";
import "./style.css";
const Radio = ({value, state}) => {
    const [stateValue, setState] = state;
    const changeHandler = (e) => {
        setState(e.target.value);
    }
    const checked = stateValue === value;
    return (
        <div>
            <input onChange={changeHandler} checked={checked} id={value} type="radio" name="suggestion-type" value={value}></input>
            <label htmlFor={value}>{value}</label>
        </div>
    )
}

const TextInput = ({id, ...rest}) => {
    return (
        <div>
            <label htmlFor={id}>{id}</label>
            <input {...rest} type="text" name={id} id={id}></input>
        </div>
    )
}

const Form = () => {
    const [input, setInput] = useState("");
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [suggestionType, setSuggestionType] = useState("");
    
    const changeHandler = (setter) => {
        return (e) => {
            setter(e.target.value);
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        const str = [name, email, suggestionType].join("\n");
        alert(str);
    }

    function reset(){
        [setInput, setName, setEmail, setSuggestionType].forEach(setter => {
            setter("");
        });
    }

    const submitEnabled = suggestionType !== "" 
        && input.length > 0;

    return (
        <form className="column" onSubmit={handleSubmit}>
            <Radio state={[suggestionType, setSuggestionType]} value="feedback"></Radio>
            <Radio state={[suggestionType, setSuggestionType]} value="suggestion"></Radio>
            <Radio state={[suggestionType, setSuggestionType]} value="question"></Radio>
            <textarea onChange={changeHandler(setInput)} value={input}></textarea>
            <TextInput onChange={changeHandler(setName)} value={name} id="name"></TextInput>
            <TextInput onChange={changeHandler(setEmail)} value={email} id="email"></TextInput>
            <button disabled={!submitEnabled} type="button" onClick={handleSubmit}>Submit</button>
            <button type="button" onClick={reset}>Reset form</button>
        </form>
    )
}

export default Form;