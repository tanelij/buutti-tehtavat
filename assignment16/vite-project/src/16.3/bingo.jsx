import { useState } from "react";
import "./style.css";

const maxNumber = 100;
const initialAvailableNumbers = Array(maxNumber).fill(0).map((n, i) => i+1);
const availableNumbers = initialAvailableNumbers;
const Bingo = () => {
    const [numbers, setNumbers] = useState([]);

    function addNumber(){
        if(availableNumbers.length === 0){
            return;
        }
        const index = Math.floor(Math.random() * availableNumbers.length);
        const number = availableNumbers.splice(index, 1)[0];
        setNumbers([...numbers, number]);
    }
    function reset(){
        setNumbers([]);
        availableNumbers = initialAvailableNumbers;
    }
    const numberElements = numbers.map(n => {
        return (
            <div className="bingo-ball" key={n}>
                {n}
            </div>
        )
    });

    return (
        <div>
            <button onClick={addNumber}>Add number</button>
            <button onClick={reset}>Reset</button>
            <div className="column">{numberElements}</div>
        </div>
    )
}

export default Bingo;