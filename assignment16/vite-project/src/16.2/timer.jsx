import { useState, useEffect } from "react";

const Timer = () => {
    const [seconds, setSeconds] = useState(0);
    const [active, setActive] = useState(true);
    useEffect(() => {
        if(!active){
            return;
        }
        const timeout = setTimeout(() => {
            setSeconds(seconds + 1);
        }, 1000);
        return () => clearTimeout(timeout);
    }, [seconds, active]);

    const minutes = Math.floor(seconds / 60);
    const remainingSeconds = seconds - (minutes * 60);
    const timeStr = `${minutes.toString()}:${remainingSeconds.toString()}`;
    
    function resetTimer(){
        setSeconds(0);
    }

    function toggleTimer(){
        setActive(!active);
    }

    function timerCallback(time){
        return () => {
            setSeconds(time);
        }
    }

    const toggleText = active
        ? "Stop" : "Start";
    return (
        <div>
            {timeStr}
            <br></br>
            <button onClick={toggleTimer}>{toggleText}</button>
            <button onClick={timerCallback(seconds + 1)}>Add second</button>
            <button onClick={timerCallback(seconds + 60)}>Add minute</button>
            <button onClick={resetTimer}>Reset</button>
        </div>
    )

}

export default Timer;