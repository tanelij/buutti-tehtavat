import { useState, createRef } from "react";
import "./style.css";

const ContactList = ({contacts, addContact, viewContact}) => {
    const [text, setText] = useState("");

    const handleChange = (e) => {
        setText(e.target.value);
    }

    const handleClick = () => {
        addContact();
    }

    const nameElems = contacts.filter(c => {
        let value = true;
        if(text !== ""){
            value = c.name.includes(text);
        }
        return value;
    }).map(c => {
        const {name} = c;
        const handleContactClick = () => {
            viewContact(c);
        }
        return <div onClick={handleContactClick} key={name}>{name}</div>
    });

    return (
        <div className="column">
            <input type="text" value={text} onChange={handleChange}/>
            {nameElems}
            <button onClick={handleClick}>Add Contact</button>
        </div>
    )
}

const ContactForm = ({contact, save, cancel}) => {
    const form = createRef()
    const textFields = contact.fields().filter(([key, value]) => {
        return key !== "notes";
    }).map(([key, value]) => {
        const valueStr = value ?? "";
        return (
            <div key={key} className="column">
                <label htmlFor={key}>{key}</label>
                <input name={key} id={key} defaultValue={valueStr} type="text"/>
            </div>
        )
    })
    const notes = (
        () => {
            const valueStr = contact.notes ?? "";
            const key = "notes";
            return (
                <div key={key} className="column">
                    <label htmlFor={key}>{key}</label>
                    <textarea name={key} defaultValue={valueStr} id={key} cols="30" rows="10"></textarea>
                </div>
            )
        }
    )();

    const fields = [...textFields, notes];
    const handleSubmit = (e) => {
        e.preventDefault();
        const elems = e.target.querySelectorAll("input[type=text],textarea")
        elems.forEach((e) => {
            contact[e.name] = e.value;
        })
        save(contact);
    }
    return (
        <form ref={form} onSubmit={handleSubmit} className="column">
            {fields}
            <div>
            <input type="submit" value="Save"></input>
            <input onClick={() => cancel()} type="button" value="Cancel"></input>
            </div>
        </form>
    )
}

const ContactView = ({edit, contact}) => {
    const listItems = contact.fields().map(([key, value]) => {
        return (
            (
                <li key={key}><i>{key}:</i> {value}</li>
            )
        )
    });

    const handleClick = () => {
        edit(contact);
    }

    return (
        <>
            <ul>
                {listItems}
            </ul>
            <button onClick={handleClick}>Edit</button>
        </>
    )
}

class Contact{
    static {
        this.idMap = new Map();
    }
    static nextId(){
        return this.idCounter++;
    }
    constructor(name, email, phone, address, website, notes){
        if(typeof(name) === "object"){
            this.init(name)
        }else{
            this.init({name, email, phone, address, website, notes})
        }
    }

    init({name, email, phone, address, website, notes}){
            this.name = name ?? "";
            this.email = email;
            this.phone = phone;
            this.address = address;
            this.website = website;
            this.notes = notes;
    }

    fields(){
        const contact = this;
        return ["name", "email", "phone", "address", "website", "notes"].map((key) => {
            return [key, contact[key]];
        })
    }

    getId(){
        Contact.idMap.get(this);
    }

    save(){
        if(!Contact.idMap.has(this)){
            Contact.idMap.set(Contact.nextId(), this);
        }
    }
}

class ContactProxy extends Contact{
    constructor(contact){
        super(contact)
        this.contact = contact;
    }

    save(){
        const contact = this.contact;
        this.contact.fields().forEach(([key, value]) => {
            contact[key] = value;
        });
        this.contact.save();
    }
}

const rightColumnStates = {
    initial: {
        view: "initial",
        title: "Contact Manager"
    },
    create: (contact) => {
        return {
            view: "create",
            title: "Add a contact",
            contact: new ContactProxy(contact)
        }
    },
    view: (contact) => {
        return {
            view: "view",
            title: contact.name,
            contact
        }
    },
    edit: (contact) => {
        return {
            view: "edit",
            title: "Edit Contact",
            contact
        }
    },
}

const Layout = () => {
    const [contacts, setContacts] = useState([]);
    const [rightColumnState, setRightColumnState] = useState(rightColumnStates.initial);

    const create = (contact) => {
        try{
            contact.save();
            setContacts([...contacts, contact]);
            setRightColumnState(rightColumnStates.initial)
        }catch(error){
            throw error;
        }
    }

    const save = (contact) => {
        contact.save();
        setContacts(contacts.slice());
        setRightColumnState(rightColumnStates.initial)
    }

    const cancel = () => {
        setRightColumnState(rightColumnStates.initial);
    }

    const addContact = () => {
        setRightColumnState(rightColumnStates.create(new Contact()));
    }
    const viewContact = (contact) => {
        setRightColumnState(rightColumnStates.view(contact));
    }
    const editContact = (contact) => {
        setRightColumnState(rightColumnStates.edit(contact));
    }

    function getRightColumnView(){
        switch(rightColumnState.view){
            case("initial"):
                return undefined;
            case("view"):
                return (
                    <ContactView edit={editContact} contact={rightColumnState.contact}></ContactView>
                )
            case("create"):
                return (
                    <ContactForm save={create} cancel={cancel} contact={rightColumnState.contact}></ContactForm>
                )
            case("edit"):
                return (
                    <ContactForm save={save} cancel={cancel} contact={rightColumnState.contact}></ContactForm>
                )
            default:
                throw "No such view!"
        }
    }


    return (
        <div className="row">
            <ContactList {...{viewContact, addContact, contacts}}></ContactList>
            <div>
                <h1>{rightColumnState.title}</h1>
                {getRightColumnView()}
            </div>
        </div>
    )
};

export default Layout;