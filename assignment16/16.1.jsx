import { useState } from "react";

const NoteForm = ({addNote}) => {
    const [str, setStr] = useState("");
    const submitHandler = (e) => {
        e.preventDefault();
        addNote(str);
        setStr("");
    }
    const changeHandler = (e) => {
        setStr(e.target.value);
    }
    return (
        <form onSubmit={submitHandler}>
            <input type="text" onChange={changeHandler} value={str}></input>
            <input type="submit"></input>
        </form>
    )
}

const Notes = ({notes, removeNote}) => {
    const noteElements = notes.map(({text, key}) => {
        const clickHandler = () => {
            removeNote(key);
        }
        return <li key={key} onClick={clickHandler}>{text}</li>
    });

    return (
        <ul>
            {noteElements}
        </ul>
    )
}

const Component = () => {
    const [notes, setNotes] = useState([]);

    function addNote(text){
        const note = {
            text,
            key: Date.now()
        }
        setNotes([...notes, note]);
    }

    function removeNote(key){
        const newNotes = notes.filter(({key: noteKey}) => {
            return key !== noteKey;
        });
        setNotes(newNotes);
    }

    return (
        <div>
            <NoteForm addNote={addNote}></NoteForm>
            <div>
                <h2>Notes: </h2>
                <Notes notes={notes} removeNote={removeNote}></Notes>
            </div>
        </div>
    )
}

export default Component;