const todoInput = document.getElementsByClassName("todo-input")[0];
const todoContainer = document.getElementsByClassName("todo")[0];
const removeButton = document.getElementsByClassName("remove-button")[0];


function addItem(str){
    const item = document.createElement("div");
    item.classList.add("todo-item");
    item.innerText = str;
    todoContainer.appendChild(item);

    item.onclick = () => {
        item.classList.toggle("checked");
    };
}

todoInput.addEventListener("keydown", (event) => {
    if(event.keyCode === 13){
        addItem(todoInput.value);
        todoInput.value = "";
    }
});

removeButton.onclick = () => {
    todoContainer.querySelectorAll(".checked")
        .forEach((item) => {
            todoContainer.removeChild(item);
        });
};