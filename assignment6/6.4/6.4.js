const paragraph = document.getElementsByTagName("p")[0];
const container = document.getElementById("container");
const text = paragraph.textContent;

function divideParagraph(str){
    const sentences = str.split(".");
    console.log(sentences);

    sentences.forEach(sentence => {
        const p = document.createElement("p");
        

        const words = sentence.split(" ");
        console.log("words ", words);
        const str = words.reduce((p, c) => {
            if(c.length > 6){
                return p + ` <span>${c}</span>`;
            }else{
                return p + " " + c;
            }
        }, "");

        p.innerHTML = str;
        container.appendChild(p);
    });
}

function removeParagraph(){
    container.removeChild(paragraph);
}

divideParagraph(text);
removeParagraph();