const textArea = document.getElementById("user-text");
const analyzer = document.getElementById("analyzer");
const analyzeButton = document.getElementById("analyze-button");

analyzeButton.onclick = () => {
    const words = Array.from(textArea.value.matchAll(/\w+/g)).map(arr => arr[0]);
    const totalWordCount = words.length;
    const averageWordLength = words.reduce(
        (length, word) => {
            return length + word.length;
        }, 0) / totalWordCount;
    const roundedAverage = Math.round(averageWordLength * 100) / 100;

    analyzer.innerText = [
        `Word count: ${totalWordCount}`,
        `Average word length: ${roundedAverage}`,
        "Word counts:"
    ].join("\n");
    analyzer.appendChild(getWordCountsList(words));
};

function getWordCountsList(words){
    const wordMap = new Map();
    words.forEach(word => {
        wordMap.set(word, 0);
    });
    words.forEach((word) => {
        wordMap.set(word, wordMap.get(word) + 1);
    });
    const wordCountList = document.createElement("ul");
    Array.from(wordMap.entries())
        .sort(([k1, v1], [k2, v2]) => {
            return v2 - v1;
        }).forEach(([k, v]) => {
            const wordItem = document.createElement("li");
            wordItem.innerText = `${k}: ${v}`;
            wordCountList.appendChild(wordItem);
        });
    return wordCountList;
}
