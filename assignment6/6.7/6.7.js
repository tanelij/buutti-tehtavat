const test = [
    1,2,3,4,
    5,6,7,8,
    9,10,16,15,
    13,14,12,11
];

class Board{
    constructor(size, element){
        this.size = size;
        this.element = element;
        this.rows = this.createPieces();
    }

    tryMovePiece(piece){
        if(piece.isEmpty){
            return;
        }
        const rowIndex = this.rows
            .findIndex(row => row.includes(piece));
        const columnIndex = this.rows[rowIndex].indexOf(piece);
        const adjacentPieces = [
            this.rows[rowIndex-1] ? this.rows[rowIndex-1][columnIndex] : undefined,
            this.rows[rowIndex+1] ? this.rows[rowIndex+1][columnIndex] : undefined,
            this.rows[rowIndex][columnIndex-1],
            this.rows[rowIndex][columnIndex+1]
        ];
        const emptyPiece = adjacentPieces.find(piece => piece?.isEmpty);

        if(emptyPiece){
            piece.isEmpty = true;
            piece.element.classList.remove("empty");
            emptyPiece.isEmpty = false;
            emptyPiece.element.classList.add("empty");
            const temp = piece.number;
            piece.number = emptyPiece.number;
            piece.element.innerText = "";
            emptyPiece.number = temp;
            emptyPiece.element.innerText = temp;
            this.checkOrder();
        }
    }

    checkOrder(){
        const inOrder = this.rows.flat(1).every((piece, i) => {
            return piece.number === (i+1);
        });

        if(inOrder){
            const piece = this.rows.flat(1)
                .find(piece => piece.isEmpty);
            piece.element.innerText = piece.number;
        }
    }

    createPieces(){
        const size = this.size;
        function* initArray(){
            for(let i=1; i<=size*size; i++){
                yield i;
            }
        }
        // const numbers = Array.from(initArray())
        //     .sort(() => {
        //         return Math.random() > 0.5 ? 1 : -1;
        //     });
        const numbers = test.reverse();
        const rows = [];
        for(let i=0; i<size; i++){
            const pieces = [];
            rows.push(pieces);
            const row = document.createElement("div");
            row.classList.add("row");
            this.element.appendChild(row);
    
            for(let j=0; j<size; j++){
                const number = numbers.pop();
                const isEmpty = number === 16;
                const piece = new Piece(number, isEmpty);
                piece.element.onclick = () => {
                    this.tryMovePiece(piece);
                };
                pieces.push(piece);
                row.appendChild(piece.element);
            }
        }

        return rows;
    }
}

class Piece{
    constructor(number, isEmpty){
        this.number = number;
        this.isEmpty = isEmpty;
        this.element = this.createElement();
    }


    createElement(){
        const piece = document.createElement("div");
        piece.setAttribute("id", this.number);
        if(!this.isEmpty){
            piece.innerText = this.number;
        }
        piece.classList.add("board__piece");
        if(this.isEmpty){
            piece.classList.add("empty");
        }
        return piece;
    }
}



const board = document.getElementById("board");
const boardObject = new Board(4, board);