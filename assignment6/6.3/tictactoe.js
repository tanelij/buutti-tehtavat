const board = document.getElementsByClassName("board")[0];
const resetButton = document.getElementById("reset-button");
let symbol = "x";
const addButton = document.getElementById("grow-board");
const reduceButton = document.getElementById("reduce-board");
let rowLength = 3;
let columnLength = 3;

function reset(){
    const squares = document.getElementsByClassName("square");
    const arr = [].slice.call(squares);

    arr.forEach((s) => {
        s.innerText = "";
    });
}

function redrawBoard(){
    board.innerHTML = "";
    for(let i=0; i<columnLength; i++){
        createRow();
    }
}

function createRow(){
    const row = document.createElement("div");
    row.classList.add("row");
    for(let i=0; i<rowLength; i++){
        const square = document.createElement("div");
        square.classList.add("square");
        row.appendChild(square);

        square.onclick = () => {
            if(square.innerText.length !== 1){
                square.innerText = symbol;
                symbol = symbol === "x" 
                    ? "o" : "x";
            }
        };
    }
    board.appendChild(row);
}

addButton.onclick = () => {
    rowLength++;
    columnLength++;
    redrawBoard();
};

reduceButton.onclick = () => {
    rowLength--;
    columnLength--;
    redrawBoard();
};

resetButton.onclick = () => {
    reset();
};

for(let i=0; i<columnLength; i++){
    createRow();
}